<?php 
include 'Views/partial/header.php';
?>
<script>
	function xoa(id,tensv) {
		var xoa=confirm("Bạn có muốn xóa "+tensv);
		if (xoa) {
			window.location.assign('<?= $siteurl."vanbang/delete/" ?>'+id);
		}
	}
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<form action="<?= $siteurl ?>" method="GET" class="form-inline" role="form">
				<input type="hidden" name="controller" value="vanbang">
				<input type="hidden" name="action" value="index">
				<div class="form-group label-floating">
					<label class="control-label" for="">Khóa tìm kiếm...</label>
					<input type="text" name="key" class="form-control" id="" >
				</div>

				

				<button type="submit" class="btn btn-primary btn-raised">Tìm kiếm</button>
			</form>
		</div>
		<div class="col-md-6 text-right">
			<a class="btn btn-primary btn-raised" data-toggle="modal" href='#modal-upload'>Upload văn bằng</a>
			<div class="modal fade" id="modal-upload">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title text-left">Upload văn bằng</h4>
						</div>
						<form action="vanbang/upload" method="POST" enctype="multipart/form-data">
							<div class="modal-body">

								<div class="form-group">
									<input type="file" id="inputFile4">
									<div class="input-group">
										<input type="text" readonly="" class="form-control" placeholder="Chọn file excel chứa thông tin văn bằng">
										<span class="input-group-btn input-group-sm">
											<button type="button" class="btn btn-fab btn-fab-mini">
												<i class="fa fa-file-excel-o"></i>
											</button>
										</span>
									</div>
								</div>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary btn-raised">Upload</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php if (count($vanbangs)): ?>
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Mã sinh viên</th>
							<th>Tên sinh viên</th>
							<th>Ngày sinh</th>
							<th>Nơi sinh</th>
							<th>Bậc đào tạo</th>
							<th>Hệ đào tạo</th>
							<th>Ngành học</th>
							<th>Số hiệu bằng</th>
							<th>Năm tốt nghiệp</th>
							<th>Ngày cấp</th>
							<th>Xếp loại</th>
							<th>Ngày upload</th>
							<th>Thao tác</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($vanbangs as $vanbang): ?>
							<tr>
								<td><?= $vanbang['masinhvien'] ?></td>
								<td><?= $vanbang['tensv'] ?></td>
								<td><?= $vanbang['ngaysinh'] ?></td>
								<td><?= $vanbang['noisinh'] ?></td>
								<td><?= $vanbang['bacdt'] ?></td>
								<td><?= $vanbang['hedaotao'] ?></td>
								<td><?= $vanbang['nganhhoc'] ?></td>
								<td><?= $vanbang['sohieubang'] ?></td>
								<td><?= $vanbang['namtotnghiep'] ?></td>
								<td><?= $vanbang['ngaycap'] ?></td>
								<td><?= $vanbang['xeploai'] ?></td>
								<td><?= $vanbang['ngayupload'] ?></td>
								<td>
									<a class="btn btn-primary btn-raised btn-sm" data-toggle="modal" href='#modal-id'>
										<i class="fa fa-pencil"></i>
									</a>
									<div class="modal fade" id="modal-id">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h4 class="modal-title">Chỉnh sửa văn bằng</h4>
												</div>
												<form action="" method="POST" role="form">
													<div class="modal-body">
														<div class="form-group">
															<label for="">Mã sinh viên</label>
															<input type="text" value="<?= $vanbang['masinhvien'] ?>" class="form-control" id="" placeholder="Input field">
														</div>
														<div class="form-group">
															<label for="">Tên sinh viên</label>
															<input type="text" value="<?= $vanbang['tensv'] ?>" class="form-control" id="" placeholder="Input field">
														</div>
														<div class="form-group">
															<label for="">Ngày sinh</label>
															<input type="text" value="<?= $vanbang['ngaysinh'] ?>" class="form-control" id="" placeholder="Input field">
														</div>
														<div class="form-group">
															<label for="">Nơi sinh</label>
															<input type="text" value="<?= $vanbang['noisinh'] ?>" class="form-control" id="" placeholder="Input field">
														</div>
														<div class="form-group">
															<label for="">Bậc đào tạo</label>
															<input type="text" value="<?= $vanbang['bacdt'] ?>" class="form-control" id="" placeholder="Input field">
														</div>
														<div class="form-group">
															<label for="">Hệ đào tạo</label>
															<input type="text" value="<?= $vanbang['hedaotao'] ?>" class="form-control" id="" placeholder="Input field">
														</div>
														<div class="form-group">
															<label for="">Ngành học</label>
															<input type="text" value="<?= $vanbang['nganhhoc'] ?>" class="form-control" id="" placeholder="Input field">
														</div>
														<div class="form-group">
															<label for="">Số hiệu bằng</label>
															<input type="text" value="<?= $vanbang['sohieubang'] ?>" class="form-control" id="" placeholder="Input field">
														</div>
														<div class="form-group">
															<label for="">Năm tốt nghiệp</label>
															<input type="text" value="<?= $vanbang['namtotnghiep'] ?>" class="form-control" id="" placeholder="Input field">
														</div>
														<div class="form-group">
															<label for="">Ngày cấp</label>
															<input type="text" value="<?= $vanbang['ngaycap'] ?>" class="form-control" id="" placeholder="Input field">
														</div>
														<div class="form-group">
															<label for="">Xếp loại</label>
															<input type="text" value="<?= $vanbang['xeploai'] ?>" class="form-control" id="" placeholder="Input field">
														</div> 



													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
														<button type="submit" class="btn btn-primary btn-raised">Lưu thay đổi</button>
													</div>
												</form>
											</div>
										</div>
									</div>
									<button onclick="xoa(<?= $vanbang['id'] ?>,'<?= $vanbang['tensv'] ?>')" class="btn btn-sm btn-danger btn-raised btn-sm"><i class="fa fa-trash"></i></button>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			<?php else: ?>
				<div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Không có kết quả nào</strong>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>
<?php 
include 'Views/partial/footer.php';
?>