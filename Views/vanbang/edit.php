<?php 
include 'Views/partial/header.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<form action="" method="POST" role="form">
				<legend>Chỉnh sửa văn bằng</legend>


				<div class="col-md-6">
					<div class="form-group">
						<label for="">Tên sinh viên</label>
						<input type="text" value="<?= $vanbang['masinhvien'] ?>" class="form-control" id="" placeholder="Input field">
					</div>
					<div class="form-group">
						<label for="">Ngày sinh</label>
						<input type="text" value="<?= $vanbang['ngaysinh'] ?>" class="form-control" id="" placeholder="Input field">
					</div>
					<div class="form-group">
						<label for="">Nơi sinh</label>
						<input type="text" value="<?= $vanbang['noisinh'] ?>" class="form-control" id="" placeholder="Input field">
					</div>
					<div class="form-group">
						<label for="">Bậc đào tạo</label>
						<input type="text" value="<?= $vanbang['bacdt'] ?>" class="form-control" id="" placeholder="Input field">
					</div>
					<div class="form-group">
						<label for="">Hệ đào tạo</label>
						<input type="text" value="<?= $vanbang['hedaotao'] ?>" class="form-control" id="" placeholder="Input field">
					</div>
					<div class="form-group">
						<label for="">Ngành học</label>
						<input type="text" value="<?= $vanbang['nganhhoc'] ?>" class="form-control" id="" placeholder="Input field">
					</div>
					<div class="form-group">
						<label for="">Số hiệu bằng</label>
						<input type="text" value="<?= $vanbang['sohieubang'] ?>" class="form-control" id="" placeholder="Input field">
					</div>
					<div class="form-group">
						<label for="">Năm tốt nghiệp</label>
						<input type="text" value="<?= $vanbang['namtotnghiep'] ?>" class="form-control" id="" placeholder="Input field">
					</div>
					<div class="form-group">
						<label for="">Ngày cấp</label>
						<input type="text" value="<?= $vanbang['ngaycap'] ?>" class="form-control" id="" placeholder="Input field">
					</div>
					<div class="form-group">
						<label for="">Xếp loại</label>
						<input type="text" value="<?= $vanbang['xeploai'] ?>" class="form-control" id="" placeholder="Input field">
					</div> 
				</div>
				<div class="col-md-6">
					<button type="submit" class="btn btn-primary">Lưu</button>
					
				</div>



			</form>
		</div>
	</div>
</div>
<?php 
include 'Views/partial/footer.php';
?>