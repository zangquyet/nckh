<?php 
include 'Views/partial/header.php';
?>
<script>
	function xoa(id,tensv) {
		var del=confirm("Bạn có muốn xóa Bảng điểm tốt nghiệp của sinh viên: "+tensv);

		if (del==true) {
			//Window.location.assign("<?= $siteurl.'sinhvien/delete/' ?>"+id);
			window.location.assign("<?= $siteurl.'diemtotnghiep/delete/' ?>"+id)
		}
	}
</script>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php if (isset($totalerror)&&count($totalerror)>0): ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Lỗi</strong>
					<?php foreach ($totalerror as $er): ?>
						<p><?= $er ?></p>
					<?php endforeach ?>
				</div>
			<?php endif ?>
			<?php if (isset($success)&&$success>0): ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Cập nhật cơ sở dữ liệu thành công</strong> <?= $success ?> điểm tốt nghiệp đã được thêm vào cơ sở dữ liệu
				</div>
			<?php endif ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row" style="margin-bottom: 10px">
		<div class="col-md-6">
			<form action="<?= $siteurl ?>index.php" method="GET" class="form-inline" role="form">
				<div class="form-group">
					<input type="hidden" name="controller" value="diemtotnghiep">
					<input type="hidden" name="action" value="index">
					<label class="sr-only" for="">Nhập từ khóa cần tìm kiếm</label>
					<input type="text" name="key" value="<?= isset($_GET['key'])?$_GET['key']:"" ?>" class="form-control" id="key" placeholder="Tên sinh viên, mã sinh viên, lớp quản lý">
				</div>
				<button type="submit" class="btn btn-raised btn-primary">Tìm kiếm</button>
			</form>
		</div>
		<div class="col-md-6 text-right">
			<a class="btn btn-primary btn-raised" data-toggle="modal" href='#modal-id'>Upload dữ liệu</a>
			<div class="modal fade text-left" id="modal-id">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Chọn file excel dữ liệu Môn Học</h4>
						</div>
						<form action="<?= $siteurl ?>diemtotnghiep/index" enctype="multipart/form-data" method="POST" role="form">
							<div class="modal-body">
								<div class="form-group is-empty is-fileinput">
								<input type="file" name="diemtotnghiep[]" id="inputFile4" multiple="">
									<div class="input-group">
										<input type="text" readonly="" class="form-control" placeholder="Bạn có thể upload nhiều file cùng lúc">
										<span class="input-group-btn input-group-sm">
											<button type="button" class="btn btn-fab btn-fab-mini">
											<i class="fa fa-file-pdf-o"></i>
											</button>
										</span>
									</div>
								</div>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
								<button type="submit" class="btn btn-raised btn-primary">Upload</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>Mã sinh viên</th>
						<th>Tên Sinh viên</th>
						<th>Lớp quản lý</th>
						<th>Ngày Upload</th>
						<th>Thao tác</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($diemtotnghieps as $diemtotnghiep): ?>
						<tr>
							<td><?= $diemtotnghiep['masinhvien'] ?></td>
							<td><?= $diemtotnghiep['tensv'] ?></td>
							<td><?= $diemtotnghiep['lop_ql'] ?></td>
							<td><?= $diemtotnghiep['date'] ?></td>
							<td>
								<a href="<?=$siteurl."uploads/diemtn/".$diemtotnghiep['masinhvien'] ?>.pdf" class="btn btn-primary btn-raised"><i class="fa fa-eye"></i></a>
								<?php if (Helper::isAdmin()): ?>
									<button onclick="xoa(<?php echo "'".$diemtotnghiep['id']."','".$diemtotnghiep['tensv']."'"; ?>)" class="btn btn-danger btn-raised"><i class="fa fa-trash"></i></button>
								<?php endif ?>

							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			Trang <?= $page."/".$totalpage ?>
			<span class="pull-right">
				<?= $phantrang ?>
			</span>
		</div>
	</div>
</div>
<?php 
include 'Views/partial/footer.php';
?>				