<?php 
include 'Views/partial/header.php';
?>
<script>
	function xoa(id,tensv) {
		var del=confirm("Bạn có muốn xóa Môn Học: "+tensv);

		if (del==true) {
			//Window.location.assign("<?= $siteurl.'sinhvien/delete/' ?>"+id);
			window.location.assign("<?= $siteurl.'monhoc/delete/' ?>"+id)
		}
	}
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php if (isset($error)&&count($error)>0): ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Lỗi</strong>
					<?php foreach ($error as $er): ?>
						<p style="color: red"><?= $er ?></p>
					<?php endforeach ?>
				</div>
			<?php endif ?>
			<?php if (isset($success)&&$success>0): ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Cập nhật cơ sở dữ liệu thành công</strong> <?= $success ?> môn học đã được thêm vào cơ sở dữ liệu
				</div>
			<?php endif ?>
			<?php if (isset($herror)&&$herror>0): ?>
				<div class="alert alert-warning">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Cảnh báo</strong> <?= $herror ?> môn học  bị trùng mã , dữ liệu những môn học này không được đưa vào DB.
				</div>
			<?php endif ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row" style="margin-bottom: 10px">
		<div class="col-md-12">
			<?php if (!empty($_SESSION['code'])): ?>
				<div class="alert alert-<?= $_SESSION['code']=="success"?"info":"danger" ?>">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong><?= $_SESSION['message'] ?></strong>
				</div>
				<?php
				unset($_SESSION['code']);
				unset($_SESSION['message']);
				endif ?>
			</div>
			<div class="col-md-6">
				<form action="<?= $siteurl ?>index.php" method="GET" class="form-inline" role="form">
					<div class="form-group">
						<input type="hidden" name="controller" value="monhoc">
						<input type="hidden" name="action" value="index">
						<label class="sr-only" for="">Nhập từ khóa cần tìm kiếm</label>
						<input type="text" name="key" value="<?= isset($_GET['key'])?$_GET['key']:"" ?>" class="form-control" id="key" placeholder="Tên môn,mã môn">
					</div>
					<button type="submit" class="btn btn-raised btn-success">Tìm kiếm</button>
				</form>
			</div>
			<div class="col-md-6 text-right">
				<a class="btn btn-raised btn-success" data-toggle="modal" href='#modal-id'>Upload dữ liệu</a>
				<div class="modal fade text-left" id="modal-id">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Chọn file excel dữ liệu Môn Học</h4>
							</div>
							<form action="<?= $siteurl ?>monhoc/index" enctype="multipart/form-data" method="POST" role="form">
								<div class="modal-body">

									<div class="form-group is-empty is-fileinput">
										<input type="file" name="monhoc" id="inputFile4">
										<div class="input-group">
											<input type="text" readonly="" name="monhoc" class="form-control" placeholder="Click here">
											<span class="input-group-btn input-group-sm">
												<button type="button" class="btn btn-fab btn-fab-mini">
													<i class="fa fa-file-excel-o"></i>
												</button>
											</span>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
									<button type="submit" class="btn btn-raised btn-primary">Upload</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-hover table-bordered">
					<thead>
						<tr>
							<th>Mã</th>
							<th>Tên Môn Học</th>
							<th>Số tín chỉ</th>
							<th>Thao tác</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($monhocs as $monhoc): ?>
							<tr>
								<td><?= $monhoc['ma'] ?></td>
								<td><?= $monhoc['ten'] ?></td>
								<td><?= $monhoc['sotinchi'] ?></td>
								<td>
									<a class="btn btn-primary btn-raised" data-toggle="modal" href='#modal-<?= $monhoc['ma'] ?>'><i class="fa fa-pencil"></i></a>
									<div class="modal fade" id="modal-<?= $monhoc['ma'] ?>">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h4 class="modal-title">Chỉnh sửa thông tin môn học</h4>
												</div>
												<form action="/monhoc/edit" method="POST" role="form">
												<input type="hidden" name="id" value="<?= $monhoc['ma'] ?>">
													<div class="modal-body">

														<div class="form-group label-floating">
															<label for="" class="control-label">Tên môn học</label>
															<input type="text" name="tenmonhoc" class="form-control" value="<?= $monhoc['ten'] ?>" id="">
														</div>
														<div class="form-group label-floating">
															<label for="" class="control-label">Số tín chỉ</label>
															<input type="number" name="sotinchi" value="<?= $monhoc['sotinchi'] ?>" class="form-control" id="">
														</div>



													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
														<button type="submit" class="btn btn-primary btn-raised">Lưu thay đổi</button>
													</div>
												</form>
											</div>
										</div>
									</div>
									<button onclick="xoa(<?php echo "'".$monhoc['ma']."','".$monhoc['ten']."'"; ?>)" class="btn btn-danger btn-raised "><i class="fa fa-trash"></i></button>

								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>


	<div class="container">
		<div class="row">
			<div class="col-md-12">
				Trang <?= $page."/".$totalpage ?>
				<span class="pull-right">
					<?= $phantrang ?>
				</span>
			</div>
		</div>
	</div>
	<?php 
	include 'Views/partial/footer.php';
	?>				