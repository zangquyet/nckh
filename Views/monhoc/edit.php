<?php include 'Views/partial/header.php'; ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row" style="margin-bottom: 50px">
		<div class="col-md-6 col-md-push-3">
			<?php if (isset($success)&&$success==true ): ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Lưu thay đổi thành công</strong>
				</div>
			<?php endif ?>
			<?php if (isset($success)&&$success==false ): ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Lưu thay đổi thất bại</strong>
				</div>
			<?php endif ?>
			<form action="" method="POST" role="form">
				<legend>Chỉnh sửa thông tin sinh viên</legend>

				<div class="form-group">
					<label for="">Tên sinh viên</label>
					<input type="text" name="ten" value="<?= $monhoc['ten'] ?>" class="form-control" id="" placeholder="Input field">
				</div>
				<div class="form-group">
					<label for="">Số tín chỉ</label>
					<input type="text" name="sotinchi" value="<?= $monhoc['sotinchi'] ?>" class="form-control" id="" placeholder="Input field">
				</div>
				<button type="submit" class="btn btn-primary btn-block">Lưu lại</button>
			</form>
		</div>
	</div>
</div>
<?php include 'Views/partial/footer.php'; ?>