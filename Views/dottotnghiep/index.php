<?php
include 'Views/partial/header.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php include 'Views/partial/alert.php'; ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row" style="padding-bottom: 20px">
		<form action="<?= $siteurl ?>">
			<input type="hidden" name="controller" value="dottotnghiep">
			<input type="hidden" name="action" value="index">
			<div class="col-md-4">
				<div class="form-group label-floating">
					<label for="i5" class="control-label">Nhập từ khóa tìm kiếm</label>
					<input type="text" value="<?= !empty($_GET['key'])?$_GET['key']:"" ?>" name="key" class="form-control" id="i5">
					<span class="help-block">Năm học, học kì,...</span>
				</div>
			</div>
			<div class="col-md-3">
				<button class="btn btn-raised btn-primary" type="submit"><i class="fa fa-search"></i> Tìm kiếm</button>
			</div>

		</form>
		<div class="col-md-3 col-md-push-2 text-right">
			<?php if (Helper::isAdmin()): ?>
				<a href="<?= $siteurl ?>dottotnghiep/add" class="btn btn-raised btn-success"><i class="fa fa-user-plus"></i> Thêm mới đợt tốt nghiệp</a>
			<?php endif ?>
		</div>
		<div class="col-md-12">
			<?php include "Views/partial/alert.php" ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th rowspan="2">ID</th>
						<th rowspan="2">Tiêu đề</th>
						<th rowspan="2">Số lượng sinh viên</th>
						<th rowspan="2">Số SV đủ điều kiện</th>
						<th rowspan="2">Trạng Thái</th>
						<th rowspan="2">Ngày bắt đầu</th>
						<th rowspan="2">Hạn cuối</th>
						<th rowspan="2">Thao tác</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$objSV=new SinhVienTotNghiep;

					foreach ($dottns as $diemtn): ?>
					<tr>
						<td><?=$diemtn['id'] ?></td>
						<td><?=$diemtn['tieude'] ?></td>
						<td><?php
							echo $objSV->gettotalsinhvien($diemtn['id']);
							?></td>
							<td><?= $objSV->gettotalsinhviendudk($diemtn['id']) ?></td>
							<td>
								<?php 
								$ngay_bd=date_parse($diemtn['ngay_bd']);
								$deadline=date_parse($diemtn['deadline']);
								$now=date_parse(Helper::getCurrentdate());

								if ($ngay_bd<=$now && $deadline>=$now) {
									echo "Đang mở";
								}
								if ($deadline<$now) {
									echo "Đã quá hạn";
								}
								if ($ngay_bd>$now) {
									echo "Chưa đến ngày duyệt";
								}
								?>
							</td>
							<td><?=Helper::getonlyDate($diemtn['ngay_bd']) ?></td>
							<td><?=Helper::getonlyDate($diemtn['deadline']) ?></td>
							<td>
								<?php if (($ngay_bd<$now && $deadline>$now) || Helper::isAdmin()): ?>

									<a href="<?= $siteurl ?>dottotnghiep/detail/<?= $diemtn['id'] ?>" class="btn btn-primary btn-raised btn-sm"><i class="fa fa-eye"></i></a>
								<?php endif ?>

								<?php if (Helper::isAdmin()): ?>
									<a href="<?= $siteurl ?>dottotnghiep/delete/<?= $diemtn['id'] ?>" class="btn btn-danger btn-raised btn-sm"><i class="fa fa-trash"></i></a>
								<?php endif ?>
								<?php if (Helper::isAdmin()): ?>
									<a class="btn btn-info btn-raised btn-sm" data-toggle="modal" href='#modal-edit<?= $diemtn['id'] ?>'><i class="fa fa-pencil"></i></a>
									<div class="modal fade" id="modal-edit<?= $diemtn['id'] ?>">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h4 class="modal-title">Chỉnh sửa đợi tốt nghiệp</h4>
												</div>
													<form action="/dottotnghiep/edit" method="POST" role="form">
												<div class="modal-body">
														<input type="hidden" name="id" value="<?= $diemtn['id'] ?>">
														<div class="form-group label-floating">
															<label for="">Tên đợt tốt nghiệp</label>
															<input type="text" name="tieude" value="<?= $diemtn['tieude'] ?>" class="form-control" id="" >
														</div>
													
														<div class="form-group label-floating">
															<label for="">Ngày bắt đầu</label>
															<input type="date" name="ngay_bd" value="<?= $diemtn['ngay_bd'] ?>" class="form-control" id="" >
														</div>
														<div class="form-group label-floating">
															<label for="">Ngày kết thúc</label>
															<input type="date" name="deadline" value="<?= $diemtn['deadline'] ?>" class="form-control" id="" >
														</div>
													
														
													
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
													<button type="submit" class="btn btn-primary btn-raised">Lưu thay đổi</button>
												</div>
													</form>
											</div>
										</div>
									</div>
								<?php endif ?>
							</td>
						</tr>
					<?php endforeach ?>

				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
include 'Views/partial/footer.php';
?>