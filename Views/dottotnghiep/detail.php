<?php
include 'Views/partial/header.php';
?>
<script>
	$(document).ready(function() {
		$("#full").hide();
		$("#check1").change(function() {
			if ($('#check1').is(":checked")) {
				$('.hocphi').attr('checked', 'checked');
			}else{
				$('.hocphi').attr('checked', false);
			}
		});

		$('.myshow').on('mousemove', function(e){
			$('#full').css({
				left:  e.pageX+50,
				top:   e.pageY+10
			});
		});
	});
	function hidetooltip() {
		$("#full").hide();
	}
	function xoa(id,tensv) {
		var del=confirm("Bạn có muốn xóa sinh viên "+ tensv);
		if (del) {
			window.location.assign('<?= $siteurl."?controller=dottotnghiep&action=xoasv&id_dtn=".$_GET['id']."&id=" ?>'+id);
		}
	}
	function update_table() {
		console.log("update table");
		$.ajax({
			url: '<?= $siteurl ?>update_ajax.php',
			type: 'POST',
			dataType: 'text',
			data: {
				action:'update_table',
				id_dtn:<?= $_GET['id'] ?>,
				page:<?= !empty($_GET['page'])?$_GET['page']:1 ?>,
				key:'<?= !empty($_GET['key'])?"{$_GET['key']}":"" ?>'
			}
		})
		.done(function(result) {

			$("#result").html(result);
			$.material.init();
			NProgress.done();
			console.log("success");
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");

		});
		
	}

	function check_hocphi(id) {
		NProgress.start();
		$.ajax({
			url: '<?= $siteurl ?>update_ajax.php',
			type: 'POST',
			dataType: 'json',
			data: {
				action: 'update_hocphi',
				myid:id},
			})
		.done(function(result) {
			console.log(result);
			console.log("success");
		})
		.fail(function(err) {
			console.log("error");
			console.log(err);
		})
		.always(function() {
			console.log("complete");
		});
		
	}
	function uncheck_hocphi(id) {
		console.log("Uncheck "+id);
		NProgress.start();
		$.ajax({
			url: '<?= $siteurl ?>update_ajax.php',
			type: 'POST',
			dataType: 'json',
			data: {
				action: 'uncheck_hocphi',
				myid:id},
			})
		.done(function(result) {

			console.log(result);
			console.log("success");
		})
		.fail(function(err) {
			console.log("error");
			console.log(err);
		})
		.always(function() {
			console.log("complete");
		});
		
	}
	
	function checkhocphi(id) {
		var iden="#"+id;
			//alert(iden);
			//alert($(iden).is(":checked"));
			if ($(iden).is(":checked")) {
				check_hocphi(id);
				update_table();
			}else{
				console.log(id);
				uncheck_hocphi(id);
				update_table();
			}
		}

		function check_tths(id) {
			NProgress.start();
			$.ajax({
				url: '<?= $siteurl ?>update_ajax.php',
				type: 'POST',
				dataType: 'json',
				data: {
					action: 'update_tths',
					myid:id},
				})
			.done(function(result) {
				console.log(result);
				console.log("success");
			})
			.fail(function(err) {
				console.log("error");
				console.log(err);
			})
			.always(function() {
				console.log("complete");
			});

		}
		function uncheck_tths(id) {
			console.log("Uncheck "+id);
			NProgress.start();
			$.ajax({
				url: '<?= $siteurl ?>update_ajax.php',
				type: 'POST',
				dataType: 'json',
				data: {
					action: 'uncheck_tths',
					myid:id},
				})
			.done(function(result) {

				console.log(result);
				console.log("success");
			})
			.fail(function(err) {
				console.log("error");
				console.log(err);
			})
			.always(function() {
				console.log("complete");
			});

		}

		function checktths(id) {
			var iden="#tths"+id;
			//alert(iden);
			//alert($(iden).is(":checked"));
			if ($(iden).is(":checked")) {
				check_tths(id);
				update_table();
			}else{
				console.log(id);
				uncheck_tths(id);
				update_table();
			}
		}
		function check_giaotrinh(id) {
			NProgress.start();
			$.ajax({
				url: '<?= $siteurl ?>update_ajax.php',
				type: 'POST',
				dataType: 'json',
				data: {
					action: 'update_giaotrinh',
					myid:id},
				})
			.done(function(result) {
				console.log(result);
				console.log("success");
			})
			.fail(function(err) {
				console.log("error");
				console.log(err);
			})
			.always(function() {
				console.log("complete");
			});

		}
		function uncheck_giaotrinh(id) {
			NProgress.start();
			$.ajax({
				url: '<?= $siteurl ?>update_ajax.php',
				type: 'POST',
				dataType: 'json',
				data: {
					action: 'uncheck_giaotrinh',
					myid:id},
				})
			.done(function(result) {
				console.log('-------------------------------------');
				console.log(result);
				console.log("success");
			})
			.fail(function(err) {
				console.log("error");
				console.log(err);
			})
			.always(function() {
				console.log("complete");
			});

		}

		function checkgiaotrinh(id) {
			var iden="#gt"+id;
			/*alert(iden);
			alert($(iden).is(":checked"));*/
			if ($(iden).is(":checked")) {
				check_giaotrinh(id);
				update_table();
			}else{
				uncheck_giaotrinh(id);
				update_table();
			}
		}
		function check_khoa(id) {
			NProgress.start();
			$.ajax({
				url: '<?= $siteurl ?>update_ajax.php',
				type: 'POST',
				dataType: 'json',
				data: {
					action: 'update_khoa',
					myid:id},
				})
			.done(function(result) {
				console.log(result);
				console.log("success");
			})
			.fail(function(err) {
				console.log("error");
				console.log(err);
			})
			.always(function() {
				console.log("complete");
			});

		}
		function uncheck_khoa(id) {
			NProgress.start();
			$.ajax({
				url: '<?= $siteurl ?>update_ajax.php',
				type: 'POST',
				dataType: 'json',
				data: {
					action: 'uncheck_khoa',
					myid:id},
				})
			.done(function(result) {

				console.log(result);
				console.log("success");
			})
			.fail(function(err) {
				console.log("error");
				console.log(err);
			})
			.always(function() {
				console.log("complete");
			});

		}

		function checkkhoa(id) {
			var iden="#khoa"+id;
			/*alert(iden);
			alert($(iden).is(":checked"));*/
			if ($(iden).is(":checked")) {
				check_khoa(id);
				update_table();
			}else{
				console.log(id);
				uncheck_khoa(id);
				update_table();
			}
		}
		function check_dang(id) {
			NProgress.start();
			$.ajax({
				url: '<?= $siteurl ?>update_ajax.php',
				type: 'POST',
				dataType: 'json',
				data: {
					action: 'update_dang',
					myid:id},
				})
			.done(function(result) {
				console.log(result);
				console.log("success");
			})
			.fail(function(err) {
				console.log("error");
				console.log(err);
			})
			.always(function() {
				console.log("complete");
			});

		}
		function uncheck_dang(id) {
			NProgress.start();
			$.ajax({
				url: '<?= $siteurl ?>update_ajax.php',
				type: 'POST',
				dataType: 'json',
				data: {
					action: 'uncheck_dang',
					myid:id},
				})
			.done(function(result) {

				console.log(result);
				console.log("success");
			})
			.fail(function(err) {
				console.log("error");
				console.log(err);
			})
			.always(function() {
				console.log("complete");
			});

		}

		function checkdang(id) {
			var iden="#dang"+id;
			/*alert(iden);
			alert($(iden).is(":checked"));*/
			if ($(iden).is(":checked")) {
				check_dang(id);
				update_table();
			}else{
				console.log(id);
				uncheck_dang(id);
				update_table();
			}
		}
		function update_tooltip(msv,tensv,lop,khoa,hocphi,hocphitime,thuvien,thuvientime,ttkhoa,ttkhoatime,dang,dangtime) {
			$("#full").show();
			$('#masinhvien').html(msv);
			$('#tensv').html(tensv);
			$('#lop').html(lop);
			$('#khoa').html(khoa);
			$('#hocphi').html(hocphi);
			$('#hocphitg').html(hocphitime);
			$('#thuvien').html(thuvien);
			$('#thuvientg').html(thuvientime);
			$('#ttkhoa').html(ttkhoa);
			$('#ttkhoatg').html(ttkhoatime);
			$('#ttdang').html(dang);
			$('#ttdangg').html(dangtime);
		}
	</script>
	<div id="full">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">
					<div id="tensv"></div>
				</h3>
			</div>
			<div class="panel-body">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<td>Mã sinh viên</td>
							<td><div id="masinhvien"></div></td>
						</tr>
						<tr>
							<td>Lớp</td>
							<td><div id="lop"></div></td>
						</tr>
						<tr>
							<td>Khoa</td>
							<td><div id="khoa"></div></td>
						</tr>
						<tr>
							<td>Tình trạng hồ sơ</td>
							<td><div id="tths"></div></td>
						</tr>
						
						<tr class="info">
							<td>Thủ tục học phí</td>
							<td><div id="hocphi"></div></td>
						</tr>
						
						<tr class="info">
							<td>Thời gian</td>
							<td><div id="hocphitg"></div></td>
						</tr>
						<tr class="success">
							<td>Thủ tục thư viện</td>
							<td><div id="thuvien"></div></td>
						</tr>
						
						<tr class="success">
							<td>Thời gian</td>
							<td><div id="thuvientg"></div></td>
						</tr>

						<tr class="warning">
							<td>Thủ tục tại khoa</td>
							<td><div id="ttkhoa"></div></td>
						</tr>
						
						<tr class="warning">
							<td>Thời gian</td>
							<td><div id="ttkhoatg"></div></td>
						</tr>
						<tr class="danger">
							<td>Thủ tục chuyển sinh hoạt Đảng</td>
							<td><div id="ttdang"></div></td>
						</tr>
						
						<tr class="danger">
							<td>Thời gian</td>
							<td><div id="ttdangtg"></div></td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?= Helper::getBreadcrum(); ?>
				<h3><?= $dtn['tieude'] ?></h3>

			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">

			<?php if (!empty($_SESSION['success'])): ?>
				<div class="col-md-12">
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>Cập nhật trạng thái thành công</strong><?= substr($_SESSION['success'], 1) ?>
					</div>
				</div>
			<?php endif ?>
			
			<?php if (!empty($_SESSION['error'])): ?>
				<div class="col-md-12">
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>Không có <?= substr($_SESSION['error'], 1) ?> trong đợt tốt nghiệp này</strong>
					</div>
				</div>
			<?php endif ?>
			<?php 
			unset($_SESSION['success']);
			unset($_SESSION['error']);
			?>
		</div>
	</div>
	<div class="container">
		
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if (!empty($_SESSION['code'])): ?>
					<?php if ($_SESSION['code']=='success'): ?>
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong><?= $_SESSION['message'] ?></strong>
						</div>
					<?php endif ?>
					<?php if ($_SESSION['code']=='error'): ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong><?= $_SESSION['message'] ?></strong>
						</div>
					<?php endif ?>
					<?php
					unset($_SESSION['code']);
					unset($_SESSION['message']);
					endif ?>
				</div>
			</div>
		</div>
		<div class="container" style="padding-bottom: 15px">
			<div class="row">
				<div class="col-md-12">
					<form action="<?= $siteurl ?>"  class="form-inline" role="form">
						<input type="hidden" name="controller" value="dottotnghiep">
						<input type="hidden" name="action" value="detail">
						<input type="hidden" name="id" value="<?= $dtn['id'] ?>">
						<div class="form-group">
							<input type="text" name="key" value="<?= !empty($_GET['key'])?$_GET['key']:"" ?>" class="form-control" id="key" placeholder="Nhập mã hoặc tên sinh viên">
						</div>
						<button type="submit" class="btn btn-primary btn-raised"><i class="fa fa-search"></i> Tìm kiếm</button>
					</form>
				</div>
			</div>
		</div>
		<div class="container" id="result">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>STT</th>
								<th>Mã</th>
								<th>Họ tên</th>
								<th>Lớp</th>
								<th>Khoa</th>
								<?php if (Helper::isTaiVu()): ?>
									<th class="hocphi"><input type="checkbox"  name="" id="check1">TTHP</th>
									<th>Thời gian</th>
								<?php endif ?>
								<?php if (Helper::isThuVien()): ?>
									<th><input type="checkbox"  name="" id="check2">TTTV</th>
									<th>Thời gian</th>
								<?php endif ?>
								<?php if (Helper::isCTSV()): ?>
									<th>TT Hồ sơ</th>
								<?php endif ?>
								<?php if (Helper::isKhoa()): ?>
									<th><input type="checkbox"  name="" id="check2">TTK</th>
									<th>Thời gian</th>
									<?php if (Helper::isKhoa()): ?>
										<th>Trạng thái hồ sơ</th>
									<?php endif ?>
								<?php endif ?>
								<?php if (Helper::isDang()): ?>
									<th><input type="checkbox"  name="" id="check2">TTCSHĐ</th>
									<th>Thời gian</th>
								<?php endif ?>

								<?php if (Helper::isAdmin()): ?>
									<td>TT Học phí</td>
									<td>TT Thư viện</td>
									<td>TT Khoa</td>
									<td>TT Chuyển SH Đảng</td>
									<td>TT hồ sơ</td>
								<?php endif ?>
							</tr>
						</thead>
						<tbody >
							<?php
							if (!empty($_GET['page'])) {
								$count=($_GET['page']-1)*50+1;
							}else {
								$count=1;
							}
							foreach ($sinhviens as $sinhvien): ?>

							<tr >
								<td><?= $count ?></td>
								<?php $count++; ?>
								<td>
									<a class="link" data-toggle="modal" href='#modal-edit-<?=$sinhvien['id']?>'><?= $sinhvien['masinhvien'] ?></a>
									<div class="modal fade" id="modal-edit-<?=$sinhvien['id']?>">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h4 class="modal-title">Chỉnh sửa thông tin sinh viên</h4>
												</div>
												<form action="/dottotnghiep/saveupdatesinhvien" method="POST">
													<div class="modal-body">
														<input type="hidden" name="id" value="<?= $sinhvien['id'] ?>">
														<input type="hidden" name="id_dtn" value="<?= $sinhvien['id_dtn'] ?>">
														<table>
															<tr>
																<td>Mã sinh viên</td>
																<td><?= $sinhvien['masinhvien'] ?></td>
															</tr>
															<tr>
																<td>Tên sinh viên</td>
																<td class="form-group"><input type="text" class="form-control" name="tensv" value="<?= $sinhvien['tensv'] ?>"></td>
															</tr>
															<tr>
																<td>Lớp</td>
																<td class="form-group"><input type="text" class="form-control" name="lop_ql" value="<?= $sinhvien['lop_ql'] ?>"></td>
															</tr>
															
															<tr>
																<td>Khoa </td>
																<td class="form-group"><input type="text" name="khoa" class="form-control" value="<?= $sinhvien['khoa'] ?>"></td>
															</tr>
															
														</table>
													</div>
													<div class="modal-footer">
														<div class="pull-left">
															<button type="button" onclick="xoa(<?= $sinhvien['id'].",'".$sinhvien['tensv']."'" ?>)" class="btn btn-danger btn-raised">Xóa</button>
														</div>
														<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
														<button type="submit" class="btn btn-primary btn-raised">Lưu chỉnh sửa</button>
													</div>
												</form>
											</div>
										</div>
									</div>
									

								</td>

								<td class="myshow" onmouseleave="hidetooltip()"
								onmouseenter="update_tooltip(<?php 

									echo "'".$sinhvien['masinhvien']."',";
									echo "'".$sinhvien['tensv']."',";
									echo "'".$sinhvien['lop_ql']."',";
									echo "'".$sinhvien['khoa']."',";


									echo $sinhvien['hocphi_stt']==1?"'Đã hoàn thành'":"'Chưa hoàn thành'";
									echo ",";
									echo ($sinhvien['taivu_time']!=NULL)?"'".$objUser->getUsernameById($sinhvien['taivu_id'])."'":"''";
									echo ",";

									echo $sinhvien['giaotrinh_stt']==1?"'Đã hoàn thành'":"'Chưa hoàn thành'";
									echo ",";
									echo ($sinhvien['thuvien_time']!=NULL)?"'".$objUser->getUsernameById($sinhvien['thuvien_id'])."'":"''";
									echo ",";


									echo $sinhvien['khoa_stt']==1?"'Đã hoàn thành'":"'Chưa hoàn thành'";
									echo ",";
									echo ($sinhvien['khoa_time']!=NULL)?"'".$objUser->getUsernameById($sinhvien['khoa_id'])."'":"''";
									echo ",";


									echo $sinhvien['dang_stt']==1?"'Đã hoàn thành'":"'Chưa hoàn thành'";
									echo ",";
									echo ($sinhvien['dang_time']!=NULL)?"'".$objUser->getUsernameById($sinhvien['dang_id'])."'":"''";
									echo "";

									?>)"
									><?= $sinhvien['tensv'] ?></td>
									<td><?= $sinhvien['lop_ql'] ?></td>
									<td><?= Helper::getshorterstring($sinhvien['khoa'],20) ?></td>
									<?php if (Helper::isTaiVu()): ?>

										<td class="hocphi">
											<div class="checkbox">
												<label>
													<input type="checkbox" <?= $sinhvien['hocphi_stt']==1?"checked":"" ?> onchange="checkhocphi(<?= $sinhvien['id'] ?>)" name="" id="<?= $sinhvien['id'] ?>">
												</label>
											</div>
										</td>
										<td>
											<?php if ($sinhvien['taivu_time']!=NULL): ?>
												Bởi <span class="label label-success"><?= $objUser->getUsernameById($sinhvien['taivu_id']) ?></span><br>

												<?= Helper::getdate($sinhvien['taivu_time']) ?>
											<?php endif ?>
										</td>
									<?php endif ?>

									<?php if (Helper::isThuVien()): ?>

										<td>
											<div class="checkbox">
												<label>
													<input type="checkbox" class="giaotrinh" <?= $sinhvien['giaotrinh_stt']==1?"checked":"" ?> onchange="checkgiaotrinh(<?= $sinhvien['id'] ?>)" name="" id="gt<?= $sinhvien['id'] ?>">
												</label>
											</div>
										</td>
										<td>
											<?php if ($sinhvien['thuvien_time']!=NULL): ?>
												Bởi <span class="label label-success"><?= $objUser->getUsernameById($sinhvien['thuvien_id']) ?></span><br>
												<?= Helper::getdate($sinhvien['thuvien_time']) ?>
											<?php endif ?>
										</td>
									<?php endif ?>
									<?php if (Helper::isCTSV()): ?>

										<td>
											<div class="checkbox">
												<label>
													<input type="checkbox" class="tths" <?= $sinhvien['tths']==1?"checked":"" ?> onchange="checktths(<?= $sinhvien['id'] ?>)" name="" id="tths<?= $sinhvien['id'] ?>">
												</label>
											</div>
										</td>
									<?php endif ?>
									<?php if (Helper::isKhoa()): ?>

										<td>
											<div class="checkbox">
												<label>
													<input type="checkbox" class="giaotrinh" <?= $sinhvien['khoa_stt']==1?"checked":"" ?> onchange="checkkhoa(<?= $sinhvien['id'] ?>)" name="" id="khoa<?= $sinhvien['id'] ?>">
												</label>
											</div>
										</td>
										<td>
											<?php if ($sinhvien['khoa_time']!=NULL): ?>
												Bởi <span class="label label-success"><?= $objUser->getUsernameById($sinhvien['khoa_id']) ?></span><br>
												<?= Helper::getdate($sinhvien['khoa_time']) ?>
											<?php endif ?>
										</td>
										<?php if (Helper::isKhoa()): ?>

											<td>
												<?php if ($sinhvien['tths']==1): ?>
													<span class="label label-success"><i class="fa fa-check"></i></span>
												<?php else: ?>
													<span class="label label-info"><i class="fa fa-times"></i></span>
												<?php endif ?>
											</td>
										<?php endif ?>
									<?php endif ?>
									<?php if (Helper::isDang()): ?>

										<td>
											<?php if ($sinhvien['is_dangvien']==1): ?>
												<div class="checkbox">
													<label>
														<input type="checkbox" class="dang" <?= $sinhvien['dang_stt']==1?"checked":"" ?> onchange="checkdang(<?= $sinhvien['id'] ?>)" name="" id="dang<?= $sinhvien['id'] ?>">
													</label>
												</div>
											<?php else: ?>
												<i class="fa fa-ban"></i>	
											<?php endif ?>

										</td>
										<td>
											<?php if ($sinhvien['dang_time']!=NULL): ?>
												Bởi <span class="label label-success"><?= $objUser->getUsernameById($sinhvien['dang_id']) ?></span><br>
												<?= Helper::getdate($sinhvien['dang_time']) ?>
											<?php endif ?>
										</td>
									<?php endif ?>

									<?php if (Helper::isAdmin()): ?>
										<td>
											<?php
											if ($sinhvien['hocphi_stt']==1) {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-success btn-raised"><i class="fa fa-check"></i> </a>
												<?php
											}else {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-raised"><i class="fa fa-times"></i> </a>
												<?php
											}

											?>
										</td>
										<td>
											<?php
											if ($sinhvien['giaotrinh_stt']==1) {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-success btn-raised"><i class="fa fa-check"></i> </a>
												<?php
											}else {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-raised"><i class="fa fa-times"></i> </a>
												<?php
											}

											?>
										</td>
										<td>
											<?php
											if ($sinhvien['khoa_stt']==1) {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-success btn-raised"><i class="fa fa-check"></i> </a>
												<?php
											}else {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-raised"><i class="fa fa-times"></i> </a>
												<?php
											}

											?>
										</td>
										<td>
											<?php
											if ($sinhvien['dang_stt']==1) {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-success btn-raised"><i class="fa fa-check"></i> </a>
												<?php
											}else {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-raised"><i class="fa fa-times"></i> </a>
												<?php
											}

											?>
										</td>
										<td>
											<?php
											if ($sinhvien['tths']==1) {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-success btn-raised"><i class="fa fa-check"></i> </a>
												<?php
											}else {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-raised"><i class="fa fa-times"></i> </a>
												<?php
											}

											?>
										</td>
									<?php endif ?>
								</tr>
							<?php endforeach ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<?= $phantrang ?>
				</div>
				<div class="col-md-3">
					<?php if (!Helper::isAdmin()): ?>
						<a class="btn btn-primary btn-raised btn-block" data-toggle="modal" href='#capnhat'><i class="fa fa-upload"></i>Import từ file</a>
					<?php else: ?>
						<a class="btn btn-success btn-raised btn-block" data-toggle="modal" href='#modal-id'><i class="fa fa-user-plus"></i> Thêm sinh viên</a>
					<?php endif ?>
				</div>
				<div class="col-md-3">
					<a class="btn btn-primary btn-raised btn-block" data-toggle="modal" href='#export'><i class="fa fa-cloud-download"></i> Export dữ liệu</a>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="card" style="padding: 20px;">
						<div class="col-md-6">
							
							<p class="text-primary"><b>SVHT thủ tục học phí : </b>
								<span class="label label-success"><?=$objSinhvienTotNghiep->gettotalsinhvienhthp($dtn['id'])?>/<?=$objSinhvienTotNghiep->gettotalsinhvien($dtn['id'])?></span>
							</p>
							<p class="text-primary"><b>SVHT thủ tục thư viện : </b>
								<span class="label label-success"><?=$objSinhvienTotNghiep->gettotalsinhvienhttrasach($dtn['id'])?>/<?=$objSinhvienTotNghiep->gettotalsinhvien($dtn['id'])?></span>
							</p>
							<p class="text-primary"><b>SVHT thủ tục Khoa : </b>
								<span class="label label-success"><?=$objSinhvienTotNghiep->gettotalsinhvienhtthutuckhoa($dtn['id'])?>/<?=$objSinhvienTotNghiep->gettotalsinhvien($dtn['id'])?></span>
							</p>
						</div>
						<div class="col-md-6">
							<p class="text-primary"><b>SVHT chuyển SH Đảng : </b>
								<span class="label label-success"><?=$objSinhvienTotNghiep->gettotalsinhvienhtthutuccshdang($dtn['id'])?>/<?=$objSinhvienTotNghiep->gettotalsinhvien($dtn['id'])?></span>
							</p>
							<p class="text-primary"><b>Tổng số đã rút hồ sơ </b>
								<span class="label label-success"><?=$objSinhvienTotNghiep->gettotalsinhvien_ruthoso($dtn['id'])?>/<?=$objSinhvienTotNghiep->gettotalsinhvien($dtn['id'])?></span>
							</p>
							<p class="text-primary"><b>Tổng số sinh viên đã đủ điều kiện tốt nghiệp </b>
								<span class="label label-success"><?=$objSinhvienTotNghiep->gettotalsinhviendudk($dtn['id'])?>/<?=$objSinhvienTotNghiep->gettotalsinhvien($dtn['id'])?></span>
							</p>
						</div>

						<div class="modal fade" id="modal-id">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Thêm mới sinh viên vào đợt tốt nghiệp này</h4>
									</div>
									<form action="<?= $siteurl ?>dottotnghiep/addsv" method="POST" role="form">

										<div class="modal-body">
											<input type="hidden" name="id_dtn" value="<?= $dtn['id'] ?>">
											<div class="form-group">
												<label for="">Mã sinh viên</label>
												<input type="text" name="masinhvien" class="form-control" id="" placeholder="">
											</div>
											<div class="form-group">
												<label for="">Tên sinh viên</label>
												<input type="text" name="tensv" class="form-control" id="" placeholder="">
											</div>
											<div class="form-group">
												<label for="">Lớp</label>
												<input type="text" name="lop_ql" class="form-control" id="" placeholder="">
											</div>
											<div class="form-group">
												<label for="">Khoa</label>
												<input type="text" name="khoa" class="form-control" id="" placeholder="">
											</div>



										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
											<button type="submit" class="btn btn-primary btn-raised"><i class="fa fa-user-plus"></i> Thêm sinh viên</button>
										</div>
									</form>

								</div>
							</div>
						</div>


						<div class="modal fade" id="export">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Export dữ liệu sinh viên</h4>
									</div>
									<form action="<?= $siteurl ?>" method="GET" role="form">
										<div class="modal-body">
											<input type="hidden" name="controller" value="dottotnghiep">
											<input type="hidden" name="action" value="export">
											<input type="hidden" name="dtn" value="<?= $_GET['id'] ?>">
											<div class="form-group">
												<label for="">Sinh viên</label>
												<select name="where" id="input" class="form-control">
													<option value="sinhviendudk">'Đã hoàn thành' tất cả các thủ tục</option>
													<option value="tatcasinhvien">Tất cả các sinh viên</option>
												</select>
											</div>
											<div class="form-group">
												<label for="">Định dạng</label>
												<select name="filetype" id="input" class="form-control">
													<option value="excel">EXCEL</option>
													<option value="pdf">PDF</option>
												</select>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-primary btn-raised"><i class="fa fa-cloud-download"></i> Export</button>
										</div>
									</form>

								</div>
							</div>
						</div>

						<?php if (!Helper::isAdmin()): ?>
							
							<div class="modal fade" id="capnhat">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title">Cập nhật tình trạng từ file</h4>
										</div>
										<form action="<?= $siteurl ?>dottotnghiep/updatefromfile" method="POST" role="form" enctype="multipart/form-data">

											<div class="modal-body">

												<div class="form-group">
													<label for="">File chứa dữ liệu sinh viên đã được hoàn thành thủ tục</label>
													<input type="file" name="fileexcel" class="form-control" id="" placeholder="Chọn file">
													<input type="hidden" name="id_dtn" value="<?= $dtn['id'] ?>">
												</div>



											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="submit" class="btn btn-primary btn-raised">Upload</button>
											</div>
										</form>

									</div>
								</div>
							</div>
						<?php endif ?>



					</div>
				</div>
			</div>
		</div>

		<?php
		include 'Views/partial/footer.php';
		?>