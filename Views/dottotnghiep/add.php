<?php 
include 'Views/partial/header.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
	<h1>Thêm đợt tốt nghiệp mới</h1>
			<?php include "Views/partial/alert.php" ?>
	</div>
	<div class="row">
		<div class="card">
			<div class="col-md-12">
				<form action="" method="POST" role="form" enctype="multipart/form-data">
					<div class="col-md-12">
						<div class="form-group">
							<label for="">Tiêu đề</label>
							<input type="text" name="tieude" class="form-control" id="" placeholder="">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="">Ngày bắt đầu</label>
							<input type="date" name="ngay_bd" class="form-control" id="" placeholder="">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<label for="">Ngày kết thúc</label>
							<input type="date" name="deadline" class="form-control" id="" placeholder="">
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
							<input type="file" name="filesv" id="inputFile4" multiple="">
							<div class="input-group">
								<input type="text" readonly="" class="form-control" placeholder="Chọn file excel chứa danh sách sinh viên tốt nghiệp">
								<span class="input-group-btn input-group-sm">
									<button type="button" class="btn btn-fab btn-fab-mini">
										<i class="fa fa-file"></i>
									</button>
								</span>
							</div>
						</div>
					</div>
					
					

					

					<div class="col-md-12" style="padding-top:20px;padding-bottom:20px;">
						<button type="submit" class="btn btn-primary btn-block btn-raised">Thêm</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php 
include 'Views/partial/footer.php';
?>