<?php 
include 'Views/partial/header.php';
?>
<script>
	function xoa(id,bangdiem) {
		var del=confirm("Bạn có muốn xóa Bảng điểm: \n	"+bangdiem);
		if (del==true) {
			//Window.location.assign("<?= $siteurl.'sinhvien/delete/' ?>"+id);
			window.location.assign("<?= $siteurl.'bangdiem/delete/' ?>"+id)
		}
	}
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">

			<?php if (isset($totalerror)&&count($totalerror)>0): ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Lỗi</strong>
					<?php foreach ($totalerror as $er): ?>
						<p><?= $er ?></p>
					<?php endforeach ?>
				</div>
			<?php endif ?>
			<?php if (isset($success)&&$success>0): ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Cập nhật cơ sở dữ liệu thành công</strong> <?= $success ?> điểm tốt nghiệp đã được thêm vào cơ sở dữ liệu
				</div>
			<?php endif ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<a class="btn btn-primary btn-raised" data-toggle="modal" href='#modal-id'><i class="fa fa-upload"></i> Upload file điểm</a>
			
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php if (!empty($_SESSION['code'])): ?>
				<div class="alert alert-<?= $_SESSION['code']=="success"?"info":"danger" ?>">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong><?= $_SESSION['message'] ?></strong>
				</div>
			<?php
			unset($_SESSION['code']);
			unset($_SESSION['message']);
			endif ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row" style="margin-bottom: 10px">
		<form action="<?= $siteurl ?>index.php" method="GET" class="" role="form">
			<input type="hidden" name="controller" value="bangdiem">
			<input type="hidden" name="action" value="index">
			<input type="hidden" name="cmd" value="search">
			<div class="col-md-2">
				<div class="form-group">
					<div class="form-group form-group label-floating">
						<label for="si4" class="control-label">Mã môn</label>
						<input type="text" name="mamon" class="form-control" id="si4" placeholder="">
						<span class="help-block">Nhập mã môn học</span>
					</div>
				</div>
				
			</div>
			<div class="col-md-2">
				
				<div class="form-group">
					<div class="form-group form-group label-floating">
						<label for="si4" class="control-label">Tên MH</label>
						<input type="text" name="tenmonhoc" class="form-control" id="si4" placeholder="">
						<span class="help-block">Nhập tên môn học</span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<div class="form-group label-floating">
						<label for="si4" class="control-label">Nhóm MH</label>
						<input type="text" name="nhom" class="form-control" id="si4" placeholder="">
						<span class="help-block">Nhập nhóm môn học</span>
					</div>
				</div>

				

			</div>
			<div class="col-md-1">

				<div class="form-group label-static">
					<select name="hocki" id="s1" class="form-control">
						<option value="">Học kì</option>
						<option value="1">01</option>
						<option value="2">02</option>
						<option value="3">03</option>
					</select>
				</div>

			</div>
			<div class="col-md-2">
				<div class="form-group">
					<select name="namhoc" id="input" class="form-control">
						<option value="">Năm Học</option>

						<?php for ($i=Helper::getyear(); $i >2000 ; $i--) { 
							?>
							<option value="<?= $i ?>"><?= $i ?></option>
							<?php
						} ?>
					</select>
				</div>
			</div>
			<div class="col-md-3" style="padding-top: 24px">
				<button type="submit" class="btn btn-primary btn-raised btn-block"><i class="fa fa-search"></i> Tìm kiếm</button>
				
			</div>
		</form>
		<div class="modal fade text-left" id="modal-id">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Chọn file excel dữ liệu Môn Học</h4>
					</div>
					<form action="<?= $siteurl ?>bangdiem/index" enctype="multipart/form-data" method="POST" role="form">
						<div class="modal-body">
							<div class="form-group">
							<input type="file" name="bangdiem[]" id="inputFile4" multiple="">

								<div class="input-group">
									<input type="text" readonly="" class="form-control" placeholder="Bạn có thể upload nhiều file">
									<span class="input-group-btn input-group-sm">
										<button type="button" class="btn btn-fab btn-fab-mini">
											<i class="fa fa-file-pdf-o"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
							<button type="submit" class="btn btn-raised btn-primary">Upload</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover table-bordered table-striped">
				<thead>
					<tr>
						<th>Mã</th>
						<th>Tên MH</th>
						<th>Nhóm MH</th>
						<th>Học Kì</th>
						<th>Năm Học</th>
						<th>Số TC</th>
						<th>Ngày Upload</th>
						<th>Thao tác</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($bangdiems as $bangdiem): 
					$pdf=explode('_', $bangdiem['filename']);
					?>
					<tr>
						<td><?= $bangdiem['ma'] ?></td>
						<td><?= $bangdiem['ten'] ?></td>
						<td><?= $pdf[1] ?></td>
						<td><?= substr($pdf[2], 5,1) ?></td>
						<td><?= substr($pdf[2], 0,4) ?></td>
						<td><?= $bangdiem['sotinchi'] ?></td>
						<td><?= Helper::getdate($bangdiem['upload_date']) ?></td>
						<td>
							<a href="<?=$siteurl."uploads/bangdiem/".$bangdiem['filename'] ?>" class="btn btn-raised btn-primary"><i class="fa fa-eye"></i></a>
							<?php if (Helper::isAdmin()): ?>
								<button onclick="xoa(<?php echo "'".$bangdiem['id']."','".$bangdiem['ten'].", Học kì: ".substr($pdf[2], 5,1).", Năm: ".substr($pdf[2], 0,4).", Lớp: ".$pdf[1]."'"; ?>)" class="btn btn-raised btn-danger"><i class="fa fa-trash"></i></button>
							<?php endif ?>

						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
</div>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			Trang <?= $page."/".$totalpage ?>
			<span class="pull-right">
				<?= $phantrang ?>
			</span>
		</div>
	</div>
</div>
<?php 
include 'Views/partial/footer.php';
?>				