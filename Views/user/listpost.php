<?php 
include 'Views/partial/header.php';
?>
<script>
	function xoa(id,title) {
		var del=confirm("Bạn có muốn xóa bài đăng: "+title);
		if (del==true) {
			window.location.assign('<?= $siteurl."post/delete/"?>'+id);
		}
	}
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<ul class="list-group">
				<li class="list-group-item">Thông tin cá nhân</li>
				<li class="list-group-item">Đổi mật khẩu</li>
				<li class="list-group-item">Danh sách bài đăng</li>
			</ul>
		</div>
		<div class="col-md-9">
			<?php foreach ($listpost as $row):
		$img =Helper::getImage($row['img_folder']);
		$imgurl=$siteurl.'uploads/'.$row['img_folder']."/".array_shift($img);
		 ?>
		<div class="col-md-4 flat-item">
			<div class="panel panel-default">
					<a href="<?= $siteurl ?>post/detail/<?= $row['id'] ?>">
					<img src="<?=$imgurl?>" class="img-responsive" alt="Image">
					</a>
				<div class="panel-footer">
					<h4><?= Helper::getshorterstring($row['tieude'],25) ?></h4>
					<h5><b>Giá: </b> <?php echo number_format($row['gia']) ?></h5>
					<h5><b>Diện tích: </b> <?= $row['dientich'] ?>m2</h5>
					<h5><b>Khu vực: </b> 

					<?php 
						$url='https://thongtindoanhnghiep.co/api/district/'.$row['huyen_id'];
						$khuvuc=file_get_contents($url);
						$kv=json_decode($khuvuc);
						echo $kv->Title." , ".$kv->TinhThanhTitle;
					 ?>
					 </h5>
					<h5><b>Trạng thái: </b>
					<?php if ($row['trangthai']==1): ?>
						<span class="label label-success">Active</span>
					<?php else: ?>
						<span class="label label-default">Deactive</span>
					<?php endif ?>

					 </h5>
					
					<div class="mypanel">
						<a href="<?= $siteurl."post/edit/".$row['id'] ?>" class="btn btn-default"><i class="fa fa-pencil-square-o"></i></a>
						<button onclick="xoa(<?= $row['id'] ?>,'<?= $row['tieude'] ?>');" class="btn btn-warning"><i class="fa fa-trash"></i></button>
						<span class="pull-right"><?= Helper::time_ago($row['add_date']) ?></span>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach ?>
		</div>
	</div>
</div>
<?php 
include 'Views/partial/footer.php';
?>