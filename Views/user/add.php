<?php include 'Views/partial/header.php'; ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row" style="margin-bottom: 50px">
		<div class="col-md-6 col-md-push-3">
			<?php if (isset($success)&&$success==true ): ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Thêm mới thành công</strong>
				</div>
			<?php endif ?>
			<?php if (isset($success)&&$success==false ): ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Thêm mới thất bại</strong>
				</div>
			<?php endif ?>
			<?php if (isset($userexist)&&$userexist==true ): ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Người dùng đã tồn tại</strong>
				</div>
			<?php endif ?>
			<form action="" method="POST" role="form">
				<legend>Thêm người dùng mới</legend>

				<div class="form-group">
					<label for="">Người dùng</label>
					<input type="text" name="username"  class="form-control" id="" placeholder="">
				</div>
				<div class="form-group">
					<label for="">Tên người dùng</label>
					<input type="text" name="name"  class="form-control" id="" placeholder="">
				</div>
				<div class="form-group">
					<label for="">Mật khẩu</label>
					<input type="text" name="password"  class="form-control" id="" placeholder="">
				</div>
				<div class="form-group">
					<label for="">Phân Quyền</label>
					<select name="type" id="inputType" class="form-control">
						<option  value="">Chọn hệ người dùng</option>
						<option  value="1">ADMIN</option>
						<option  value="2">Khai thác viên</option>
						<option  value="3">Tài vụ</option>
						<option  value="4">Thư viện</option>
						<option  value="5">Khoa</option>
						<option  value="6">Quản lý đang viên</option>
						<option  value="7">Phòng công tác sinh viên</option>
					</select>
				</div>
				<button type="submit" class="btn btn-success btn-block">Thêm</button>
			</form>
		</div>
	</div>
</div>
<?php include 'Views/partial/footer.php'; ?>