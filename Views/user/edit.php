<?php include 'Views/partial/header.php'; ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row" style="margin-bottom: 50px">
		<div class="col-md-6 col-md-push-3">
			<?php if (isset($success)&&$success==true ): ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Lưu thay đổi thành công</strong>
				</div>
			<?php endif ?>
			<?php if (isset($success)&&$success==false ): ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Lưu thay đổi thất bại</strong>
				</div>
			<?php endif ?>
			<form action="" method="POST" role="form">
				<legend>Chỉnh sửa thông tin sinh viên</legend>

				<div class="form-group">
					<label for="">Người dùng</label>
					<input type="text" name="username" value="<?= $user['username'] ?>" class="form-control" id="" placeholder="Input field">
				</div>
				<div class="form-group">
					<label for="">Tên người dùng</label>
					<input type="text" name="name" value="<?= $user['name'] ?>" class="form-control" id="" placeholder="Input field">
				</div>
				<div class="form-group">
					<label for="">Phân Quyền</label>
					<select name="type" id="inputType" class="form-control">
						<option <?= $user['type']==1?"selected":"" ?> value="1">ADMIN</option>
						<option <?= $user['type']==2?"selected":"" ?> value="2">Người khai thác</option>
						<option <?= $user['type']==3?"selected":"" ?> value="3">Tài vụ</option>
						<option <?= $user['type']==4?"selected":"" ?> value="4">Thư viện</option>
						<option <?= $user['type']==5?"selected":"" ?> value="5">Khoa</option>
						<option <?= $user['type']==6?"selected":"" ?> value="6">Quản lý văn phòng Đảng</option>
						<option <?= $user['type']==7?"selected":"" ?> value="7">Thư viện</option>
					</select>
				</div>
				<button type="submit" class="btn btn-primary btn-block">Lưu lại</button>
			</form>
		</div>
	</div>
</div>
<?php include 'Views/partial/footer.php'; ?>