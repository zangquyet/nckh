<?php 
include 'Views/partial/header.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<ul class="list-group">
				<li class="list-group-item">Thông tin cá nhân</li>
				<li class="list-group-item">Đổi mật khẩu</li>
				<li class="list-group-item">Danh sách bài đăng</li>
			</ul>
		</div>
		<div class="col-md-9">
		<?php if (isset($error) && in_array('passnotright', $error)): ?>

		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Thất bại</strong> mật khẩu cũ không chính xác!
		</div>
		<?php endif ?>
		<?php if (isset($error) && in_array('passnotmatch', $error)): ?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Thất bại</strong> mật khẩu mới không trùng khớp!
		</div>
		<?php endif ?>
		<?php if (isset($success) && $success=true): ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Đổi mật khẩu thành công</strong>
		</div>
		<?php endif ?>

			<form action="" method="POST" role="form">
				<legend>Thay đổi mật khẩu</legend>
			
				<div class="form-group">
					<label for="">Mật khẩu cũ</label>
					<input type="password" name="old" class="form-control" id="" placeholder="">
				</div>
				<div class="form-group">
					<label for="">Mật khẩu mới</label>
					<input type="password" name="new" class="form-control" id="" placeholder="">
				</div>
				<div class="form-group">
					<label for="">Nhập lại mật khẩu mới</label>
					<input type="password" name="new2" class="form-control" id="" placeholder="">
				</div>
			
				
			
				<button type="submit" class="btn btn-primary">Submit</button>
				<div style="margin-bottom:100px"></div>
			</form>
		</div>
	</div>
</div>
<?php 
include 'Views/partial/footer.php';
?>