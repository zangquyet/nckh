<?php 
include 'Views/partial/header.php';
?>
<script>
	function xoa(id,name) {
		var del=confirm("Bạn có muốn xóa người dùng"+name);
		if (del==true) {
			window.location.assign("<?= $siteurl ?>"+"user/delete/"+id);
		}
	}
</script>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?= Helper::getBreadcrum(); ?>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-6">
			<form action="" method="GET" class="form-inline" role="form">

				<div class="form-group label-floating">
					<label class="control-label">Nhập từ khóa tìm kiếm</label>
					<input type="text" class="form-control" id="">
				</div>

				

				<button type="submit" class="btn btn-primary btn-raised">Tìm kiếm</button>
			</form>
		</div>
		<div class="col-md-6 text-right">
			<a class="btn btn-primary btn-raised" data-toggle="modal" href='#modal-add'>Thêm người dùng mới</a>
			<div class="modal fade" id="modal-add">
				<div class="modal-dialog">
					<div class="modal-content text-left">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Thêm mới người dùng</h4>
						</div>
						<form action="/user/add" method="POST" role="form">
							<div class="modal-body">

								<div class="form-group">
									<label for="">Người dùng</label>
									<input type="text" name="username"  class="form-control" id="" placeholder="">
								</div>
								<div class="form-group">
									<label for="">Tên người dùng</label>
									<input type="text" name="name"  class="form-control" id="" placeholder="">
								</div>
								<div class="form-group">
									<label for="">Mật khẩu</label>
									<input type="text" name="password"  class="form-control" id="" placeholder="">
								</div>
								<div class="form-group">
									<label for="">Phân Quyền</label>
									<select name="type" id="inputType" class="form-control">
										<option  value="">Chọn hệ người dùng</option>
										<option  value="1">ADMIN</option>
										<option  value="2">Khai thác viên</option>
										<option  value="3">Tài vụ</option>
										<option  value="4">Thư viện</option>
										<option  value="5">Khoa</option>
										<option  value="6">Quản lý đang viên</option>
										<option  value="7">Phòng công tác sinh viên</option>
									</select>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary">Save changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php if (!empty($_SESSION['code'])): ?>
				<div class="alert alert-<?= $_SESSION['code']=="success"?"info":"danger" ?>">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong><?= $_SESSION['message'] ?></strong>
				</div>
				<?php
				unset($_SESSION['code']);
				unset($_SESSION['message']);
				endif ?>
			</div>
			<div class="col-md-12">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Tên người dùng</th>
							<th>Tên</th>
							<th>Quyền người dùng</th>
							<th>Thao tác</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($users as $user): ?>
							<tr>
								<td><?= $user['id'] ?></td>
								<td><?= $user['username'] ?></td>
								<td><?= $user['name'] ?></td>
								<td><?php
									switch ($user['type']) {
										case 1:
										echo "Admin";
										break;
										case 2:
										echo "Khai Thác viên";
										break;
										case 3:
										echo "Tài vụ";
										break;
										case 4:
										echo "Thư viện";
										break;
										case 5:
										echo "Khoa";
										break;
										case 6:
										echo "Quản lý đảng viên";
										break;
										case 7:
										echo "CTSV";
										break;
										default:
										echo "Lỗi";
										break;
									}
									?></td>
									<td>
										<a class="btn btn-primary btn-raised" data-toggle="modal" href='#modal-edit<?= $user['id'] ?>'><i class="fa fa-pencil"></i></a>
										<div class="modal fade" id="modal-edit<?= $user['id'] ?>">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
														<h4 class="modal-title">Chỉnh sửa thông tin người dùng</h4>
													</div>
													<form action="/user/edit" method="POST" role="form">
														<div class="modal-body">
															<input type="hidden" name="id" value="<?= $user['id'] ?>">
															<div class="form-group label-floating">
																<label for="" class="control-label">Người dùng</label>
																<input type="text" name="username" value="<?= $user['username'] ?>" class="form-control" id="">
															</div>

															<div class="form-group label-floating">
																<label for="" class="control-label">Tên người dùng</label>
																<input type="text" name="name" value="<?= $user['name'] ?>" class="form-control" id="">
															</div>

															<div class="form-group label-floating">
																<div class="form-group">
																	<label for="">Phân Quyền</label>
																	<select name="type" id="inputType" class="form-control">
																		<option <?=$user['type']==1?"selected=''":""?> value="1">ADMIN</option>
																		<option <?=$user['type']==2?"selected=''":""?> value="2">Người khai thác</option>
																		<option <?=$user['type']==3?"selected=''":""?> value="3">Tài vụ</option>
																		<option <?=$user['type']==4?"selected=''":""?> value="4">Thư viện</option>
																		<option <?=$user['type']==5?"selected=''":""?> value="5">Khoa</option>
																		<option <?=$user['type']==6?"selected=''":""?> value="6">Quản lý văn phòng Đảng</option>
																		<option <?=$user['type']==7?"selected=''":""?> value="7">Thư viện</option>
																	</select>
																</div>
															</div>



														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
															<button type="submit" class="btn btn-primary btn-raised">Save changes</button>
														</div>
													</form>
												</div>
											</div>
										</div>
										<button onclick="xoa(<?= $user['id'].",'".$user['username']."'" ?>)" class="btn btn-danger btn-raised"><i class="fa fa-trash"></i></button>
									</td>
								</tr>
							<?php endforeach ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php 
		include 'Views/partial/footer.php';
		?>