<?php 
$siteurl=Helper::$siteurl;
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<!-- Latest compiled and minified CSS & JS -->
	<!-- <link rel="stylesheet" href="<?= $siteurl ?>style.css"> -->
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="<?= $siteurl ?>md/css/bootstrap-material-design.min.css">
	<link rel="stylesheet" href="<?= $siteurl ?>md/css/ripples.min.css">
	<link rel="stylesheet" href="<?= $siteurl ?>md/css/snackbar.min.css">
	<link rel="stylesheet" href="<?= $siteurl ?>style.css">
	<script src="<?= $siteurl ?>md/js/material.min.js"></script>
	<script src="<?= $siteurl ?>md/js/ripples.min.js"></script>
	<script src="<?= $siteurl ?>md/js/snackbar.min.js"></script>
	<script src='<?= $siteurl ?>src/nprogress.js'></script>
	<link rel='stylesheet' href='<?= $siteurl ?>src/nprogress.css'/>


	<link rel="shortcut icon" href="http://nuce.edu.vn/sites/default/files/logo_dhxd_tttt.png" type="image/png" />


	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


	<link href="//cdn.rawgit.com/FezVrasta/dropdown.js/master/jquery.dropdown.css" rel="stylesheet">
	<script>
		$(document).ready(function() {
			$.material.init();
		});
	</script>
	<title>NUCE</title>
</head>
<body>
	<header>
		<div class="logo">
			<div class="container" style="padding: 10px 0px;">
				<a href="<?=$siteurl ?>">
					<img src="<?= $siteurl ?>img/logo.png" class="img-responsive" alt="Image">
				</a>
			</div>
		</div>		

		<div class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?= $siteurl ?>">NUCE</a>
				</div>
				<div class="navbar-collapse collapse navbar-responsive-collapse">
					<ul class="nav navbar-nav">
					<li><a href="<?= $siteurl ?>page/timkiemvanbang">Tra cứu văn bằng</a></li>
						<?php if (Helper::isKTV()): ?>
							<li class="dropdown">
							<a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Tìm kiếm
								<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a  href="<?= $siteurl ?>diemtotnghiep/index">Bảng điểm tốt nghiệp</a></li>
									<li><a  href="<?= $siteurl ?>bangdiem/index">Bảng điểm học kì</a></li>
								</ul>
							</li>
						<?php endif ?>
						<?php if (Helper::isAdmin()): ?>
							<li class="dropdown">
							<a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Quản lý
								<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a  href="<?= $siteurl ?>sinhvien/index">Quản lý sinh viên</a></li>
									<li><a  href="<?= $siteurl ?>monhoc/index">Quản lý Môn học</a></li>
									<li><a  href="<?= $siteurl ?>vanbang/index">Quản lý văn bằng </a></li>
									<li><a  href="<?= $siteurl ?>user/index">Quản lý Người dùng</a></li>
									<li><a  href="<?= $siteurl ?>diemtotnghiep/index">Quản lý Điểm tốt nghiệp</a></li>
									<li><a  href="<?= $siteurl ?>bangdiem/index">Quản lý Bảng điểm</a></li>
								</ul>
							</li>
						<?php endif ?>
							<?php if (Helper::islogin()&&!Helper::isKTV()): ?>
								<li class="dropdown">
								<a href="#" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Xét duyệt tốt nghiệp
									<b class="caret"></b></a>
									<ul class="dropdown-menu">
										<?php if (Helper::isAdmin()): ?>
											<li><a  href="<?= $siteurl ?>dottotnghiep/add">Thêm đợt mới</a></li>
										<?php else: ?>
												<li>

												<a href="javascript:void(0)" data-content="Bạn không có quyền truy cập vào chức năng này" data-toggle="snackbar" data-timeout="0">Thêm đợt mới</a>


												</li>

										<?php endif ?>
										<li><a  href="<?= $siteurl ?>dottotnghiep/index">Danh sách tất cả các đợt</a></li>
									</ul>
								</li>
							<?php endif ?>
							</ul>

							<?php if (Helper::islogin()): ?>
								<ul class="nav navbar-nav navbar-right">
								<li class="dropdown">
									<a href="http://fezvrasta.github.io/bootstrap-material-design/bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"> </i> <?= $_SESSION['username'] ?>
										<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a href="/page/changepass">Đổi mật khẩu</a></li>
											<li class="divider"></li>
											<li><a href="<?= $siteurl ?>page/logout">Đăng xuất</a></li>
										</ul>
									</li>
								</ul>
							<?php else: ?>
								<ul class="nav navbar-nav navbar-right">
									<li><a href="<?= $siteurl ?>page/login" ><i class="fa fa-user"> </i> Đăng nhập</a></li>
								</ul>
							<?php endif ?>
							</div>
						</div>
					</div>



				</header>

