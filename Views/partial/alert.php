<?php if (!empty($_SESSION['code'])): ?>
	<div class="alert alert-<?= $_SESSION['code']=="success"?"info":"danger" ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong><?= $_SESSION['message'] ?></strong>
	</div>
	<?php
	unset($_SESSION['code']);
	unset($_SESSION['message']);
	endif ?>