<?php 
include 'Views/partial/header.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-12 text-center">
			<i style="font-size: 10em;color: #f1c40f" class="fa fa-exclamation-triangle" aria-hidden="true"></i>
			<h1 style="font-weight: bold">Forbidden</h1>
			<h3>Bạn không có quyền truy cập vào trang này ,<a href="<?php echo siteurl ?>">Quay lại trang chủ</a></h3>
		</div>
	</div>
</div>

 <?php 
include 'Views/partial/footer.php';
 ?>