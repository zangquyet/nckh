<?php 
include 'Views/partial/header.php';
?>
<div class="container">
	<div class="col-md-6 col-md-push-3">
		<?php 
			if (isset($just_reg) && $just_reg==true) {
				?>
				<div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Đăng kí thành công</strong> Đăng nhập ngay
				</div>
				<?php
			}
			?>
			<?php 
			if (isset($login) && $login==false) {
				?>
				<div class="alert alert-warning">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Đăng nhập thất bại</strong> thử lại
				</div>
				<?php
			}
			?>
        <div class="well bs-component">
          <form class="form-horizontal" method="POST">
            <fieldset>
              <legend>Đăng nhập</legend>
              <div class="form-group is-empty">
                <label for="inputEmail" class="col-md-2 control-label">Username</label>

                <div class="col-md-10">
                  <input type="text" name="username" class="form-control" id="inputEmail" placeholder="" tabindex="1" required="">
                </div>
              </div>
              <div class="form-group is-empty">
                <label for="inputPassword" class="col-md-2 control-label">Password</label>

                <div class="col-md-10">
                  <input type="password" name="pass" class="form-control" tabindex="2" id="inputPassword" placeholder="" required="">

         
              <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                  <button type="reset" class="btn btn-default">Reset</button>
                  <button type="submit" class="btn btn-raised btn-primary" tabindex="3">Đăng nhập</button>
                </div>
              </div>
            </fieldset>
          </form>
        <div id="source-button" class="btn btn-primary btn-xs" style="display: none;">&lt; &gt;</div></div>
      </div>
</div>


<?php 
include 'Views/partial/footer.php';
?>