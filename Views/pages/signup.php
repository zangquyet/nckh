<?php 
include "Views/partial/header.php"
 ?>
<div class="container">
	<div class="row">
		<div class="col-md-6 text-center">
			<i class="fa fa-user" style="font-size: 10em;"></i>
			<h3>Đăng kí mới người dùng</h3>
		</div>
		<div class="col-md-6">
			<form action="signupsubmit" method="POST" role="form">
				<div class="form-group">
					<label for="">Username</label>
					<?php
					//echo isset($error);
					//echo in_array('invalidemail', $error) ;
					if (isset($error) && in_array('issetuser', $error)) {
					?>
					<span class="error">Người dùng đã tồn tại</span>
					<?php
					}
					 ?>
					<input type="text" name="username" value="<?php echo isset($_POST['username'])?$_POST['username']:"" ?>" class="form-control" id="" placeholder="">
				</div>
				<div class="form-group">
					<label for="">Password</label>
						<?php
					//echo isset($error);
					//echo in_array('invalidemail', $error) ;
					if (isset($error) && in_array('passnotmatch', $error)) {
					?>
					<span class="error">Mật khẩu không trùng khớp</span>
					<?php
					}
					 ?>
					<input type="password" name="pass" class="form-control" id="" placeholder="">
				</div>
				<div class="form-group">
					<label for="">Nhập lại password</label>
					<input type="password" name="pass2" class="form-control" id="" placeholder="">
				</div>
				<div class="form-group">
					<label for="">Email</label>

					<?php
					//echo isset($error);
					//echo in_array('invalidemail', $error) ;
					if (isset($error) && in_array('invalidemail', $error)) {
					?>
					<span class="error">Email không hợp lệ</span>
					<?php
					}
					 ?>
					<input type="text" value="<?php echo isset($_POST['email'])?$_POST['email']:"" ?>" name="email" class="form-control" id="" placeholder="">
				</div>
				<button type="submit" class="btn btn-success btn-block">Submit</button>
			</form>
		</div>
	</div>
</div>
 <?php 
include "Views/partial/footer.php"
 ?>