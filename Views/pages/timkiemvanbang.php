<?php 
include 'Views/partial/header.php';
?>
<script>
	$(document).ready(function() {
		
		/*$.snackbar({content: 'This is a JS generated snackbar, it rocks!', style: 'toast'});*/
	});
	function findbymsv() {
		/*$.snackbar({content: "This is my awesome snackbar!"});	*/
		NProgress.start();
		$.ajax({
			url: '<?= $siteurl."vanbang_ajax.php" ?>',
			type: 'POST',
			dataType: 'text',
			data: {
				find:'msv',
				msv:$("#masinhvien").val()
			},
		})
		.done(function(result) {
			NProgress.done();


			$("#result").html(result)
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		return false;
	}
	function findbyvanbang() {
		NProgress.start();

		$.ajax({
			url: '<?= $siteurl."vanbang_ajax.php" ?>',
			type: 'POST',
			dataType: 'text',
			data: {
				find:'vanbang',
				vanbang:$("#vanbang").val()
			},
		})
		.done(function(result) {
			NProgress.done();

			$("#result").html(result);

		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		return false;
	}
</script>



<div class="container" style="margin-bottom: 100px;">
	<h3><i class="fa fa-search"></i> TRA CỨU VĂN BẰNG</h3>
	<div role="tabpanel" class="card">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" id="vanbang" role="tablist">
			<li role="presentation" class="active">
				<a href="#msv" aria-controls="msv" role="tab" data-toggle="tab">Mã sinh viên</a>
			</li>
			<li role="presentation">
				<a href="#mavanbang" aria-controls="mavanbang" role="tab" data-toggle="tab">Số hiệu văn bằng</a>
			</li>
			<li role="presentation">
				<a href="#hoten" aria-controls="hoten" role="tab" data-toggle="tab">Họ tên và ngày sinh</a>
			</li>
		</ul>

		<!-- Tab panes -->
		<div class="col-md-12">
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="msv">
					<form action="" onsubmit="return findbymsv()" method="POST" role="form">

						<div class="form-group">
							<input type="text" class="form-control" id="masinhvien" placeholder="Mã sinh viên">
						</div>



						<button type="submit" class="btn btn-primary pull-right btn-raised">Tìm kiếm</button>
					</form>
				</div>
				<div role="tabpanel" class="tab-pane" id="mavanbang">
					<form action="" onsubmit="return findbyvanbang()" method="POST" role="form">
						
						<div class="form-group">
							<input type="text" class="form-control" id="vanbang" placeholder="Số hiệu văn bằng">
						</div>
						
						
						
						<button type="submit" onclick="" class="btn btn-primary pull-right btn-raised">Tìm kiếm</button>
					</form>
				</div>
				<div role="tabpanel" class="tab-pane" id="hoten">
					<form action="" onsubmit="return findbyvanbang()" method="POST" role="form">
						
						<div class="form-group">
							<input type="text" class="form-control" id=hoten"" placeholder="Họ tên sinh viên">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="ngaysinh" placeholder="Ngày tháng năm sinh">
						</div>
						
						
						
						<button type="submit" class="btn btn-primary pull-right btn-raised">Tìm kiếm</button>
					</form>
				</div>
			</div>
		</div>
		
	</div>

</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div id="result">
			</div>
		</div>
	</div>
</div>
<div style="padding: 50px 0px"></div>
<?php 
include 'Views/partial/footer.php';
?>