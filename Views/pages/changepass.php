<?php 
include 'Views/partial/header.php';
?>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-push-3">
			<h3><i class="fa fa-key"></i> Thay đổi mật khẩu</h3>
			<?php if (isset($success) &&$success): ?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Đổi mật khẩu thành công</strong> 
				</div>
			<?php endif ?>
			
			<?php if (isset($success) &&!$success): ?>

			<div class="alert alert-warning">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>Đổi mật khẩu không thành công</strong>
				<?php foreach ($error as $err): ?>
					<p style="color: red"><?= $err ?></p>
				<?php endforeach ?>
			</div>
			<?php endif ?>

			<div class="panel panel-primary">

				<div class="panel-body">
					<form action="<?php echo $siteurl ?>page/changepass" method="POST" role="form">
						<div class="form-group">
							<label for="">Mật khẩu cũ</label>
							<input type="Password" name="old" class="form-control" id="" placeholder="">
						</div>
						<div class="form-group">
							<label for="">Mật khẩu mới</label>
							<input type="Password" name="new" class="form-control" id="" placeholder="">
						</div>
						<div class="form-group">
							<label for="">Nhập lại mật khẩu mơi</label>
							<input type="Password" name="new2" class="form-control" id="" placeholder="">
						</div>
						
						<button type="submit" class="btn btn-success">Đổi mật khẩu</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
include 'Views/partial/footer.php';
?>