<?php 
if ($_SERVER["REQUEST_METHOD"]=="POST") {
	include 'Classes/Vanbang.php';
	include 'Classes/Db.php';
	$objVanBang=new VanBang;
	if ($_POST['find']=='msv') {
		if ($objVanBang->issetvanbangmasv($_POST['msv'])) {
			$vanbang=$objVanBang->getVanBangbymsv($_POST['msv']);
			?>
			<table class="table table-bordered table-hover card">
				<thead>
					<tr>
						<th>Mã sinh viên</th>
						<th>Tên sinh viên</th>
						<th>Ngày sinh</th>
						<th>Nơi sinh</th>
						<th>Bậc đào tạo</th>
						<th>Hệ đào tạo</th>
						<th>Ngành học</th>
						<th>Số hiệu bằng</th>
						<th>Năm tốt nghiệp</th>
						<th>Ngày cấp</th>
						<th>Xếp loại</th>
						<th>Ngày upload</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?= $vanbang['masinhvien'] ?></td>
						<td><?= $vanbang['tensv'] ?></td>
						<td><?= $vanbang['ngaysinh'] ?></td>
						<td><?= $vanbang['noisinh'] ?></td>
						<td><?= $vanbang['bacdt'] ?></td>
						<td><?= $vanbang['hedaotao'] ?></td>
						<td><?= $vanbang['nganhhoc'] ?></td>
						<td><?= $vanbang['sohieubang'] ?></td>
						<td><?= $vanbang['namtotnghiep'] ?></td>
						<td><?= $vanbang['ngaycap'] ?></td>
						<td><?= $vanbang['xeploai'] ?></td>
						<td><?= $vanbang['ngayupload'] ?></td>

					</tr>
				</tbody>
			</table>
			<?php
		}else {
			?>
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>Không có sinh viên này</strong> Thử lại ...
			</div>
			<?php
		}
	}else if ($_POST['find']=='vanbang') {
		if ($objVanBang->issetvanbangmavb($_POST['vanbang'])) {
			$vanbang=$objVanBang->getVanBangbyshb($_POST['vanbang']);
			?>
			<table class="table table-bordered table-hover card">
				<thead>
					<tr>
						<th>Mã sinh viên</th>
						<th>Tên sinh viên</th>
						<th>Ngày sinh</th>
						<th>Nơi sinh</th>
						<th>Bậc đào tạo</th>
						<th>Hệ đào tạo</th>
						<th>Ngành học</th>
						<th>Số hiệu bằng</th>
						<th>Năm tốt nghiệp</th>
						<th>Ngày cấp</th>
						<th>Xếp loại</th>
						<th>Ngày upload</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?= $vanbang['masinhvien'] ?></td>
						<td><?= $vanbang['tensv'] ?></td>
						<td><?= $vanbang['ngaysinh'] ?></td>
						<td><?= $vanbang['noisinh'] ?></td>
						<td><?= $vanbang['bacdt'] ?></td>
						<td><?= $vanbang['hedaotao'] ?></td>
						<td><?= $vanbang['nganhhoc'] ?></td>
						<td><?= $vanbang['sohieubang'] ?></td>
						<td><?= $vanbang['namtotnghiep'] ?></td>
						<td><?= $vanbang['ngaycap'] ?></td>
						<td><?= $vanbang['xeploai'] ?></td>
						<td><?= $vanbang['ngayupload'] ?></td>

					</tr>
				</tbody>
			</table>
			<?php
		}else {
			?>
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>Không có sinh viên này</strong> Thử lại ...
			</div>
			<?php
		}
	}
}
?>