<?php 
/**
* 
*/
class UserController
{
	public function index()
	{
		$active="quanly";
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		$users=$objUser->all();
		include 'Views/user/index.php';
	}
	public function edit()
	{
		$active="quanly";
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		$objUser=new User;
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$data=array(
				'username'=>$_POST['username'],
				'name'=>$_POST['name'],
				'type'=>$_POST['type']
				);	
			if ($objUser->update($_POST['id'],$data)) {
				$_SESSION['code']="success";
				$_SESSION['code']="Cập nhật người dùng thành công";
				Helper::redirect(Helper::$siteurl."user/index");
			}else {
				$success=false;
			}
		}
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objUser->issetUserID($id)) {
				$user=$objUser->getUserById($id);
				include 'Views/user/edit.php';
			}else {
				//do smth
			}
			
		}else {
			//do somthing
		}
	}
	public function add()
	{
		$active="quanly";
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$objUser=new User;

			if ($objUser->issetUser($_POST['username'])) {
				$_SESSION['code']='error';
					$_SESSION['message']='Người dùng đã tồn tại';
					Helper::redirect(Helper::$siteurl."user/index");
			}else{
				$data=array(
					'username'=>$_POST['username'],
					'password'=>md5($_POST['password']),
					'type'=>$_POST['type'],
					'name'=>$_POST['name']
					);	
				if ($objUser->add($data)) {
					$_SESSION['code']='success';
					$_SESSION['message']='Thêm người dùng mới thành công';
					Helper::redirect(Helper::$siteurl."user/index");
				}else {
					echo "Thất bại";
				}
			}
			
		}
		include 'Views/user/add.php';
	}
	public function changepass()
	{
		$active="quanly";
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		SESSION::start();
		$objUser=new User;
		if (!$objUser->islogin()) {
			Helper::redirect(Helper::$siteurl."page/login");
		}
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$error=[];
			$old=$_POST['old'];
			$new=$_POST['new'];
			$new2=$_POST['new2'];
			if ($new!=$new2) {
				$error[]="passnotmatch";
			}

			if (!$objUser->isUser($_SESSION['username'],$old)) {
				$error[]="passnotright";
			}
			if (count($error)==0) {
				if ($objUser->updatepassword($_SESSION['id'],$new)) {
					$success=true;
				}
			}
		}
		include "Views/user/changepass.php";
	}
	public function delete()
	{
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		$objUser=new User;
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objUser->issetUserID($id)) {
				if ($objUser->delete($id)) {
					$_SESSION['code']="success";
				$_SESSION['message']="Xóa người dùng thành công";
				Helper::redirect(Helper::$siteurl."user/index");
				}
			}else {
				include 'Views/pages/404.php';
			}
		}else {
			include 'Views/pages/404.php';
		}
	}
}
?>