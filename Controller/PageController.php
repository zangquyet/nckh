<?php 
/**
* 
*/
class PageController
{
	public function login()
	{
		session_start();
		$active="login";
		if (!empty($_SESSION['id'])) {
			Helper::redirect(Helper::$siteurl);
		}
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$username=$_POST['username'];
			$pass=$_POST['pass'];
			$objUser=new User();
			if ($objUser->isUser($username,$pass)) {
				$user=$objUser->getUserByUsername($username);
				Session::set('id',$user['id']);
				Session::set('username',$user['username']);
				Session::set('type',$user['type']);
				if (Helper::isAdmin()) {
					Helper::redirect(Helper::get_admin_after_login_site());
				}else if (Helper::isKTV()) {
					Helper::redirect(Helper::get_ktv_after_login_site());
				}else {
					Helper::redirect(Helper::get_admin_after_login_site());
				}

			}else{
				$login=false;
			}
		}
		include 'Views/pages/login.php';
	}
	public function timkiemvanbang()
	{
		$active="timkiemvanbang";
		include 'Views/pages/timkiemvanbang.php';
	}
	public function signup()
	{
		session_start();
		if (!empty($_SESSION['id'])) {
			Helper::redirect(Helper::$siteurl);
		}
		include 'Views/pages/signup.php';
	}
	public function signupsubmit()
	{
		$error=[];
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$username=$_POST["username"];			
			$pass=$_POST["pass"];			
			$pass2=$_POST["pass2"];			
			$email=$_POST["email"];
			if ($pass!=$pass2) {
				$error[]="passnotmatch";
			}
			$objUser=new User();

			if ($objUser->issetUser($username)>0) {
				$error[]='issetuser';
			}

			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$error[]="invalidemail";
			}

			if (count($error)>0) {
				include 'Views/pages/signup.php';
				//$this->signup();
			}else {
				$data=array(
					'username'=>$username,
					'password'=>md5($pass),
					'email'=>$email
					);
				//echo "Register Success";
				//Helper::redirect();
				if ($objUser->add($data)) {
					$just_reg=true;
					include 'views/pages/login.php';
				}else {
					echo "False";
				}
			}
		}
	}


	public function logout()
	{
		Session::start();
		Session::destroy();
		Helper::redirect(Helper::$siteurl);
	}
	public function changepass()
	{
		session_start();
		if (!Helper::islogin()) {
			Helper::redirect(Helper::$siteurl."/page/login");
		}
		$id=$_SESSION['id'];
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$success=true;
			$old=$_POST['old'];	
			$new=$_POST['new'];	
			$new2=$_POST['new2'];	
			if ($new!=$new2) {
				$success=false;
				$error[]="Mật khẩu không trùng khớp";
			}
			$objUser=new User;

			$user=$objUser->getUserById($id);
			if (md5($old)!=$user['password']) {
				$success=false;
				$error[]="Mật khẩu cũ không chính xác";
			}
			if ($success) {
				$objUser->updatepassword($id,$new);
			}
		}
		include 'Views/pages/changepass.php';
	}
}
?>