<?php
/**
* 
*/
class DotTotNghiepController
{
	public function add()
	{
		Session::start();
		if (!Helper::isAdmin()) {
			include 'Views/pages/permission.php';
			return;
		}

		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			if ($_FILES['filesv']['name']!="") {
				$error=array();
				$target_dir = "uploads/tmp/";
				$data=array(
					'tieude'=>$_POST['tieude'],
					'deadline'=>$_POST['deadline'],
					'ngay_bd'=>$_POST['ngay_bd']
					);
				$objDotTotNghiep=new DotTotNghiep;
				$objDotTotNghiep->add($data);
				$last_id=$objDotTotNghiep->getlastid();
				$objPHPExcel=new PHPExcel;

				$target_file = $target_dir . basename($_FILES["filesv"]["name"]);
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

				if ($_FILES["filesv"]["size"] > 5000000) {
					$error[] = "Tệp PDF quá lớn";
					$uploadOk = 0;
				}
				if ($imageFileType != "xls") {
					$error[] = "Tệp sai định dạng";
					$uploadOk = 0;
				}
				if ($uploadOk == 0) {
					$error[] = "Tệp chưa được upload";
				} else {
					if (move_uploaded_file($_FILES["filesv"]["tmp_name"], $target_file)) {
						$objPhpexcel=new PHPExcel;
						$tmpfname = $target_file;
						$excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
						$excelObj = $excelReader->load($tmpfname);
						$worksheet = $excelObj->getSheet(0);
						$lastRow = $worksheet->getHighestRow();

						for ($row = 1; $row <= $lastRow; $row++) {
							$masinhvien = $worksheet->getCell('B' . $row)->getValue();
							$tensinhvien = $worksheet->getCell('C' . $row)->getValue();
							$lop_ql = $worksheet->getCell('D' . $row)->getValue();
							$khoa = $worksheet->getCell('E' . $row)->getValue();
							$isdangvien = trim($worksheet->getCell('F' . $row)->getValue());
							$dv=$isdangvien=='x'?1:0;
							$objSinhvienTotNghiep=new SinhVienTotNghiep;

							$data = array(
								'masinhvien' => $masinhvien,
								'tensv' => $tensinhvien,
								'lop_ql' => $lop_ql,
								'khoa' => $khoa,
								'id_dtn' => $last_id,
								'is_dangvien'=>$dv
								);
							if ($objSinhvienTotNghiep->add($data)) {
								//
							}else {
								//
							}
						}
						unlink($target_file);
						$_SESSION['code']="success";
						$_SESSION['message']="Thêm đợi mới thành công";
						Helper::redirect(Helper::$siteurl."dottotnghiep/index");

					} else {
						$error[] = "Tệp chưa được upload";
					}
				}

			}else{
				$_SESSION['code']="success";
				$_SESSION['message']="Bạn chưa chọn file";
			}
			
		}
		$active='xettn';
		include 'Views/dottotnghiep/add.php';
	}
	public function addsv()
	{
		session_start();
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$objSinhvienTotNghiep=new SinhVienTotNghiep;
			if ($objSinhvienTotNghiep->add($_POST)) {
				$_SESSION['code']="success";		
				$_SESSION['message']="Thêm mới sinh viên thành công";
				Helper::redirect(Helper::$siteurl.'dottotnghiep/detail/'.$_POST['id_dtn']);
			}else {
				$_SESSION['code']="error";
				$_SESSION['message']="Thêm mới sinh viên không thành công";
				Helper::redirect(Helper::$siteurl.'dottotnghiep/detail/'.$_POST['id_dtn']);
			}
		}
	}
	public function index()
	{
		$objDotTotNghiep=new DotTotNghiep;
		
		if (!empty($_GET['key'])) {
			$where="tieude like '%{$_GET['key']}%'";
			
			$dottns=$objDotTotNghiep->getlist($where);
			include 'Views/dottotnghiep/index.php';
			return;
		}
		
		$active='xettn';
		
		if (!empty($_GET['id'])) {
			$page=$_GET['id'];
		}else {
			$page=1;
		}
		$dottns=$objDotTotNghiep->all($page);
		include 'Views/dottotnghiep/index.php';
	}
	public function edit()
	{
		session_start();
		$objDotTotNghiep=new DotTotNghiep;
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$data=array(
				'tieude'=>$_POST['tieude'],
				'ngay_bd'=>$_POST['ngay_bd'],
				'ngay_bd'=>$_POST['ngay_bd'],
				'deadline'=>$_POST['deadline']
				);	
			if ($objDotTotNghiep->update($_POST['id'],$data)) {
				$_SESSION['code']="success";
				$_SESSION['message']="Sửa thông tin thành công";
				Helper::redirect(Helper::$siteurl."dottotnghiep/index");
			}else {
				$success=false;
			}
		}
	}
	public function detail()
	{
		Session::start();
		if (!Helper::islogin()||Helper::isKTV()) {
			include 'Views/pages/permission.php';
			return;
		}
		$objUser=new User;
		$active="xettn";
		$objSinhvienTotNghiep=new SinhVienTotNghiep;
		$objDotTotNghiep=new DotTotNghiep;

		if (!empty($_GET['page'])) {
			$page=$_GET['page'];
		}else {
			$page=1;
		}
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			$dtn=$objDotTotNghiep->getdtn($id);
			if (!empty($_GET['key'])) {
				$where="id_dtn=".$id." AND (tensv LIKE '%{$_GET['key']}%' OR masinhvien LIKE '%{$_GET['key']}%')";
			}else {
				$where="id_dtn=".$id;
			}
			$sinhviens=$objSinhvienTotNghiep->getList($where,$page);
			$url=Helper::$siteurl."?controller=dottotnghiep&action=detail&id={$id}&page=";
			$current=$page;
			$total=$objSinhvienTotNghiep->getpage();
			$phantrang=Helper::getPagination($url,$current,$total);
		}else {
			include 'Views/pages/404.php';
			return;
		}
		include 'Views/dottotnghiep/detail.php';
	}
	public function delete()
	{
		session_start();

		$objDotTotNghiep=new DotTotNghiep;
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objDotTotNghiep->issetDTN($id)) {
				if ($objDotTotNghiep->delete($id)) {
					$objSinhvienTotNghiep=new SinhVienTotNghiep;
					$objSinhvienTotNghiep->delete_id_dtn($id);
					$_SESSION['code']="success";
					$_SESSION['message']="Xóa thành công";
					Helper::redirect(Helper::$siteurl."dottotnghiep/index");
				}
			}else {
				// error
			}
		}else {
			//error
		}
	}


	public function updatefromfile()
	{
		if ($_SERVER['REQUEST_METHOD']=="POST") {
			$objSinhvienTotNghiep=new SinhVienTotNghiep;
			session_start();
			$id_dtn=$_POST['id_dtn'];
			$success=0;
			$herror=0;
			$error=array();
			$target_dir = "uploads/tmp";
			$objPHPExcel=new PHPExcel;

			$target_file = $target_dir . basename($_FILES["fileexcel"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

			if ($_FILES["fileexcel"]["size"] > 5000000) {
				$error[] = "Tệp PDF quá lớn";
				$uploadOk = 0;
			}
			if ($imageFileType != "xlsx") {
				$error[] = "Tệp sai định dạng";
				$uploadOk = 0;
			}
			if ($uploadOk == 0) {
				$error[] = "Tệp chưa được upload";
			} else {
				if (move_uploaded_file($_FILES["fileexcel"]["tmp_name"], $target_file)) {
					$objPhpexcel=new PHPExcel;
					$tmpfname = $target_file;
					$excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
					$excelObj = $excelReader->load($tmpfname);
					$worksheet = $excelObj->getSheet(0);
					$lastRow = $worksheet->getHighestRow();
					$_SESSION['success']="";
					$_SESSION['error']="";
					for ($row = 1; $row <= $lastRow; $row++) {
						$masinhvien = $worksheet->getCell('A' . $row)->getValue();

						if ($masinhvien!="" || $masinhvien!=NULL) {
							if (Helper::isThuVien()) {
								$data=array(
									'giaotrinh_stt'=>1,
									'thuvien_id'=>$_SESSION['id'],
									'thuvien_time'=>Helper::getCurrentdate()
									);
							}
							if (Helper::isKhoa()) {
								$data=array(
									'khoa_stt'=>1,
									'khoa_id'=>$_SESSION['id'],
									'khoa_time'=>Helper::getCurrentdate()
									);
							}
							if (Helper::isDang()) {
								$data=array(
									'dang_stt'=>1,
									'dang_id'=>$_SESSION['id'],
									'dang_time'=>Helper::getCurrentdate()
									);
							}
							if (Helper::isTaiVu()) {
								$data=array(
									'hocphi_stt'=>1,
									'taivu_id'=>$_SESSION['id'],
									'taivu_time'=>Helper::getCurrentdate()
									);
							}

							if (Helper::isCTSV()) {
								$data=array(
									'tths'=>1
									);
							}

							if ($objSinhvienTotNghiep->isset($masinhvien)) {
								if ($objSinhvienTotNghiep->update($id_dtn,$masinhvien,$data)) {
									$_SESSION['success'].=",".$masinhvien;
								}
							}else {
								$_SESSION['error'].=",".$masinhvien;
							}
						}
					}
				} else {
					$error[] = "Tệp chưa được upload";
				}
				Helper::redirect(Helper::$siteurl."dottotnghiep/detail/{$id_dtn}");
			}
			unlink($target_file);
		}
	}
	public function xoasv()
	{
		$id=$_GET['id'];
		$id_dtn=$_GET['id_dtn'];
		$sv=new SinhVienTotNghiep;
		if ($sv->delete($id)) {
			session_start();
			$_SESSION['code']="success";
			$_SESSION['message']="Xóa sinh viên thành công";
			Helper::redirect(Helper::$siteurl."DotTotNghiep/detail/".$id_dtn);
		}

	}
	public function saveupdatesinhvien()
	{
		$id=$_POST['id'];
		$id_dtn=$_POST['id_dtn'];
		$data=array(
			'tensv'=>$_POST['tensv'],
			'lop_ql'=>$_POST['lop_ql'],
			'khoa'=>$_POST['khoa']
			);
		$sv=new SinhVienTotNghiep;
		if ($sv->update_theo_id($id,$data)) {
			session_start();
			$_SESSION['code']="success";
			$_SESSION['message']="Cập nhật sinh viên thành công";
			Helper::redirect(Helper::$siteurl."DotTotNghiep/detail/".$id_dtn);
		}
	}

	public function export()
	{
		$objSinhvienTotNghiep=new SinhVienTotNghiep;
		$objDotTotNghiep=new DotTotNghiep;
		$dtn_id=$_GET['dtn'];
		$dtn=$objDotTotNghiep->getdtn($dtn_id);
		$type=$_GET['filetype'];
		$where=$_GET['where'];
		if ($type=="pdf") {
			echo "Đang xây dựng chức năng này";
		}else if ($type=="excel") {
			switch ($where) {
				case 'sinhviendudk':
				$sinhvien=$objSinhvienTotNghiep->getSinhvienTotNghiepDuDK($dtn_id);
				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getActiveSheet()->setCellValue('A1','Mã sinh viên');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Tên sinh viên');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Lớp quản lý');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Khoa');
				$i=2;
				foreach ($sinhvien as $sv) {
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$sv['masinhvien']);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$sv['tensv']);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$sv['lop_ql']);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$sv['khoa']);
					$i++;
				}
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("C1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("D1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->setTitle('Danh sách sinh viên');
				ob_clean();
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="helloworld.xlsx"');
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objWriter->save('php://output');

				break;
				
				case 'tatcasinhvien':
				$sinhvien=$objSinhvienTotNghiep->getall($dtn_id);
				/*print_r($sinhvien);
				die();*/
				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getActiveSheet()->setCellValue('A1','Mã sinh viên');
				$objPHPExcel->getActiveSheet()->setCellValue('B1','Tên sinh viên');
				$objPHPExcel->getActiveSheet()->setCellValue('C1','Lớp quản lý');
				$objPHPExcel->getActiveSheet()->setCellValue('D1','Khoa');
				$objPHPExcel->getActiveSheet()->setCellValue('E1','Hoàn thành học phí');
				$objPHPExcel->getActiveSheet()->setCellValue('F1','Hoàn thành thủ tục thư viện');
				$objPHPExcel->getActiveSheet()->setCellValue('G1','Hoàn thành thủ tục khoa');
				$objPHPExcel->getActiveSheet()->setCellValue('H1','Hoàn thành thủ tục chuyển SHĐ');
				$objPHPExcel->getActiveSheet()->setCellValue('I1','Đảng viên');
				$objPHPExcel->getActiveSheet()->setCellValue('J1','Trạng thái hồ sơ');
				$i=2;
				foreach ($sinhvien as $sv) {
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$sv['masinhvien']);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$sv['tensv']);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$sv['lop_ql']);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$sv['khoa']);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$sv['hocphi_stt']==1?"x":"");
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$sv['giaotrinh_stt']==1?"x":"");
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$sv['khoa_stt']==1?"x":"");
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$sv['dang_stt']==1?"x":"");
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$sv['is_dangvien']==1?"x":"");
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i,$sv['tths']==1?"x":"");
					$i++;
				}
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("C1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("D1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("E1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("F1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("G1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("H1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("I1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyle("J1")->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->setTitle('Danh sách sinh viên');
				ob_clean();
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="DS_sinhvien.xlsx"');
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
				$objWriter->save('php://output');

				break;
				
				default:
					# code...
				break;
			}
			
		}
	}
}
?>
