<?php
/**
* 
*/
class DiemTotNghiepController
{
	public function index()
	{
		$objDiemTotNghiep=new DiemTotNghiep;
		$success=0;
		$active='quanly';
		Session::start();
		if ($_SERVER["REQUEST_METHOD"]=="POST") {

			$error = array();
			$target_dir = "uploads/diemtn/";
			for ($i = 0; $i < count($_FILES['diemtotnghiep']['name']); $i++) {
				$target_file = $target_dir . basename($_FILES["diemtotnghiep"]["name"][$i]);
				$uploadOk = 1;
				$Filetype = pathinfo($target_file, PATHINFO_EXTENSION);
				$msv=explode('.', basename($_FILES["diemtotnghiep"]["name"][$i]));
				$msv=array_shift($msv);
				if ($_FILES["diemtotnghiep"]["size"][$i] > 5000000) {
					$error[] = "file qúa lớn";
					$uploadOk = 0;
				}
				if ($Filetype != "pdf") {
					$error[] = "không đúng định dạng";
					$uploadOk = 0;
				}
				if ($objDiemTotNghiep->issetDTN($msv)) {
					$error[] = "đã tồn tại trong hệ thống";
					$uploadOk = 0;
				}
				if ($uploadOk == 0) {

					$fault = implode(",", $error);
					$loix= $_FILES["diemtotnghiep"]["name"][$i]. " " . $fault;
					$totalerror[] = "<b>".$_FILES["diemtotnghiep"]["name"][$i]."</b> ".$fault;
					$fault="";
					unset($error);
				} else {
					if (move_uploaded_file($_FILES["diemtotnghiep"]["tmp_name"][$i], $target_file)) {
						$data = array(
							'name' => basename($_FILES["diemtotnghiep"]["name"][$i]),
							'id_user' => $_SESSION['id'],
							'masinhvien'=>$msv
							);
						if ($objDiemTotNghiep->add($data)) {
							$success++;
						}
					}
				}
			}

			//print_r($totalerror);
		}
		if (!empty($_GET['key'])) {
			if (!empty($_GET['page'])) {
				$page=$_GET['page'];
			}else {
				$page=1;
			}

			$where="name like '%".$_GET['key']."%' ";
			$where.="OR tensv like '%".$_GET['key']."%' ";
			$where.="OR lop_ql like '%".$_GET['key']."%' ";
			$diemtotnghieps=$objDiemTotNghiep->getlist($where,$page);
			$totalpage=$objDiemTotNghiep->getpagewhere($where);
			$url=Helper::$siteurl."?controller=diemtotnghiep&action=index&key={$_GET['key']}&page=";
			$phantrang=Helper::getPagination($url,$page,$totalpage);
			include 'Views/diemtotnghiep/index.php';
		}else{
			if (!empty($_GET['id'])) {
				$page=$_GET['id'];
			}else {
				$page=1;
			}
			$objDiemTotNghiep=new DiemTotNghiep;
			$diemtotnghieps=$objDiemTotNghiep->all($page);
			if (count($diemtotnghieps)==0) {
				//Helper::redirect(Helper::$siteurl);
			}
			$totalpage=$objDiemTotNghiep->getpage();
			$url=Helper::$siteurl."diemtotnghiep/index/";
			$phantrang=Helper::getPagination($url,$page,$totalpage);
			include 'Views/diemtotnghiep/index.php';
		}

	}
	public function edit()
	{
		$active="quanly";
		$objMonhoc=new MonHoc;
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$data=array(
				'tensv'=>$_POST['tensv'],
				'lop_ql'=>$_POST['lop_ql']
				);	
			if ($objDiemTotNghiep->update($_GET['id'],$data)) {
				$success=true;
			}else {
				$success=false;
			}
		}
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objMonhoc->issetMonhoc($id)) {
				$monhoc=$objMonhoc->getMonhoc($id);
				include 'Views/sinhvien/edit.php';

			}else {
				//do smth
			}

		}else {
			//do somthing
		}
	}
	public function delete()
	{
		$objDiemTotNghiep=new DiemTotNghiep;
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objDiemTotNghiep->issetDiemTotNghiep($id)) {
				$dtn=$objDiemTotNghiep->getDiemTotNghiep($id);
				if ($objDiemTotNghiep->delete($id)) {
					$file="uploads/diemtn/".$dtn['name'];

					if (unlink($file)) {
						echo "Xóa file trên server thành công <br>";
					}else{
						echo "Xóa file trên server thành công <br>";
					}
					echo "Xóa dữ liệu trong CSDL Thành công <a href=\"".Helper::$siteurl."diemtotnghiep/index\">Click để quay lại</a>";

				}
			}else {
				// error
			}
		}else {
			//error
		}
	}
}
?>
