<?php
/**
* 
*/
class BangDiemController
{
	public function index()
	{
		
		$active='quanly';
		$objBangDiem=new BangDiem;
		$success=0;
		
		Session::start();
		if ($_SERVER["REQUEST_METHOD"]=="POST") {

			$error = array();
			$target_dir = "uploads/bangdiem/";
			for ($i = 0; $i < count($_FILES['bangdiem']['name']); $i++) {
				$target_file = $target_dir . basename($_FILES["bangdiem"]["name"][$i]);
				$uploadOk = 1;
				$Filetype = pathinfo($target_file, PATHINFO_EXTENSION);
				$filename=basename($_FILES["bangdiem"]["name"][$i]);
				$split=explode('_', $filename);
				if ($_FILES["bangdiem"]["size"][$i] > 5000000) {
					$error[] = "file qúa lớn";
					$uploadOk = 0;
				}
				if ($Filetype != "pdf") {
					$error[] = "không đúng định dạng";
					$uploadOk = 0;
				}
				if ($objBangDiem->issetBD($filename)) {
					$error[] = "đã tồn tại trong hệ thống";
					$uploadOk = 0;
				}
				if ($uploadOk == 0) {

					$fault = implode(",", $error);
					$loix= $_FILES["bangdiem"]["name"][$i]. " " . $fault;
					$totalerror[] = "<b>".$_FILES["bangdiem"]["name"][$i]."</b> ".$fault;
					$fault="";
					unset($error);
				} else {
					if (move_uploaded_file($_FILES["bangdiem"]["tmp_name"][$i], $target_file)) {
						$data = array(
							'filename' => basename($_FILES["bangdiem"]["name"][$i]),
							'subject_code' => array_shift($split),
							'nhom' => array_shift($split),
							'namhoc' => substr($split[0], 0,4),
							'hocki' => substr($split[0], -6,-1),
							'id_user' => $_SESSION['id']
							);
						if ($objBangDiem->add($data)) {
							$success++;
						}
					}
				}
			}

			//print_r($totalerror);
		}

		if (!empty($_GET['cmd'])&&$_GET['cmd']=='search') {
			if (!empty($_GET['page'])) {
				$page=$_GET['page'];
			}else {
				$page=1;
			}
			$mamonhoc=!empty($_GET['mamon'])?$_GET['mamon']:"";
			$tenmonhoc=!empty($_GET['tenmonhoc'])?$_GET['tenmonhoc']:"";
			$nhom=!empty($_GET['nhom'])?$_GET['nhom']:"";
			$hocki=!empty($_GET['hocki'])?$_GET['hocki']:"";
			$namhoc=!empty($_GET['namhoc'])?$_GET['namhoc']:"";
			$where="";
			$where.="subject_code LIKE '%{$mamonhoc}%' ";
			$where.="AND ten LIKE '%{$tenmonhoc}%' ";
			$where.="AND nhom LIKE '%{$nhom}%' ";
			$where.="AND hocki LIKE '%{$hocki}%' ";
			$where.="AND namhoc LIKE '%{$namhoc}%' ";
			//echo $where;
			$totalpage=$objBangDiem->getpagewhere($where);
			$url=Helper::$siteurl."?controller=bangdiem&action=index&cmd=search&mamon={$mamonhoc}&tenmonhoc={$tenmonhoc}&nhom={$nhom}&hocki={$hocki}&namhoc={$namhoc}&id=";
			$phantrang=Helper::getPagination($url,$page,$totalpage);
			$bangdiems=$objBangDiem->getlist($where,$page);
			include 'Views/bangdiem/index.php';
		}else{
			if (!empty($_GET['id'])) {
				$page=$_GET['id'];
			}else {
				$page=1;
			}
			$objBangDiem=new BangDiem;
			$bangdiems=$objBangDiem->all($page);
			if (count($bangdiems)==0) {
				//Helper::redirect(Helper::$siteurl);
			}
			$totalpage=$objBangDiem->getpage();
			$url=Helper::$siteurl."bangdiem/index/";
			$phantrang=Helper::getPagination($url,$page,$totalpage);
			include 'Views/bangdiem/index.php';
		}

	}
	public function edit()
	{
		$active='quanly';
		$objMonhoc=new MonHoc;
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$data=array(
				'tensv'=>$_POST['tensv'],
				'lop_ql'=>$_POST['lop_ql']
				);	
			if ($objBangDiem->update($_GET['id'],$data)) {
				$success=true;
			}else {
				$success=false;
			}
		}
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objMonhoc->issetMonhoc($id)) {
				$monhoc=$objMonhoc->getMonhoc($id);
				include 'Views/sinhvien/edit.php';

			}else {
				//do smth
			}

		}else {
			//do somthing
		}
	}
	public function delete()
	{
		$objBangDiem=new BangDiem;
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			if ($objBangDiem->issetbangdiem($id)) {
				$bangdiem=$objBangDiem->getbangdiem($id);
				$file="uploads/bangdiem/".$bangdiem['filename'];
				//die($file);
				if ($objBangDiem->delete($id)) {

					if (unlink($file)) {
						$msg="Xóa file trên server thành công <br>";
					}else{
						$msg= "Xóa file trên server thất bại <br>";
					}
					$msg.= "Xóa dữ liệu trong CSDL Thành công";
					session_start();
					$_SESSION['code']="success";
					$_SESSION['message']=$msg;
					Helper::redirect(Helper::$siteurl."bangdiem/index");
				}
			}else {
				// error
			}
		}else {
			//error
		}
	}
}
?>
