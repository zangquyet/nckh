<?php 
/**
* 
*/
class VanBangController
{
	public function index()
	{
		SESSION::start();
		$objUser=new User;
		$objVanbang=new VanBang;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		if (!empty($_GET['id'])) {
			$page=$_GET['id'];
		}else {
			$page=1;
		}
		if (!empty($_GET['key'])) {
			$key=$_GET['key'];
			$vanbangs=$objVanbang->find($key,$page);
			$url=Helper::$siteurl."?controller=vanbang&action=index&key={$key}&id=";
		}else {
			
			$vanbangs=$objVanbang->all($page);
			$url=Helper::$siteurl."?controller=vanbang&action=index&id=";
		}
		$phantrang=Helper::getPagination($url,$page,$total);
		include 'Views/vanbang/quanlyvanbang.php';
	}
	public function suavanbang()
	{
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		$active='quanly';
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			$objVanbang=new VanBang;
			if ($objVanbang->issetvanBang($id)) {
				$vanbang=$objVanbang->getVanbang($id);
				include 'Views/vanbang/edit.php';				
			}
		}else {
			//redirect error
		}

	}
	public function delete()
	{
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		if (!empty($_GET['id'])) {
			$id=$_GET['id'];
			$objVanbang=new Vanbang;
			if ($objVanbang->issetvanBang($id)) {
				if ($objVanbang->xoavb($id)) {
					Helper::redirect(Helper::$siteurl."vanbang/index");
				}

			}
		}else {
			//redirect error
		}
	}
}
?>
