<?php
/**
* 
*/
class MonHocController
{
	public function index()
	{
		$active='quanly';
		SESSION::start();
		if (!Helper::isAdmin()) {
			include 'Views/pages/permission.php';
			return;
		}
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$success=0;
			$herror=0;
			$error = array();
			$target_dir = "uploads/tmp";
			$target_file = $target_dir . basename($_FILES["monhoc"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

			if ($_FILES["monhoc"]["size"] > 5000000) {
				$error[] = "Tệp PDF quá lớn";
				$uploadOk = 0;
			}
			if ($imageFileType != "xlsx") {
				$error[] = "Tệp sai định dạng";
				$uploadOk = 0;
			}
			if ($uploadOk == 0) {
				$error[] = "Tệp chưa được upload";
			} else {
				if (move_uploaded_file($_FILES["monhoc"]["tmp_name"], $target_file)) {
            //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
					$objPhpexcel=new PHPExcel;
					$tmpfname = $target_file;
					$excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
					$excelObj = $excelReader->load($tmpfname);
					$worksheet = $excelObj->getSheet(0);
					$lastRow = $worksheet->getHighestRow();

					for ($row = 1; $row <= $lastRow; $row++) {
						$ma = $worksheet->getCell('A' . $row)->getValue();
						$ten = $worksheet->getCell('B' . $row)->getValue();
						$stc = $worksheet->getCell('C' . $row)->getValue();
						$objMonHoc=new MonHoc;

						$data = array(
							'ma' => $ma,
							'ten' => $ten,
							'sotinchi' => $stc
							);
						if (!$objMonHoc->issetMonHoc($ma)) {
							if ($objMonHoc->add($data)) {
								$success++;
							}else {
								$herror++;
							}
						} else {
							$herror++;
						}
					}
				} else {
					$error[] = "Tệp chưa được upload";
				}
			}
			unlink($target_file);
		}
		if (!empty($_GET['id'])) {
				$page=$_GET['id'];
			}else {
				$page=1;
			}
		if (!empty($_GET['key'])) {
			$objMonHoc=new MonHoc;
			$where="ma like '%".$_GET['key']."%' ";
			$where.=" OR ten like '%".$_GET['key']."%' ";
			$monhocs=$objMonHoc->getlist($where,$page);
			$totalpage=$objMonHoc->getpagewhere($where);
			$url=Helper::$siteurl."?controller=monhoc&action=index&key={$_GET['key']}&page=";
			$phantrang=Helper::getPagination($url,$page,$totalpage);
			include 'Views/monhoc/index.php';
		}else{
			
			$objMonHoc=new MonHoc;
			$monhocs=$objMonHoc->all($page);
			if (count($monhocs)==0) {
				//Helper::redirect(Helper::$siteurl);
			}
			$totalpage=$objMonHoc->getpage();
			$url=Helper::$siteurl."monhoc/index/";
			$phantrang=Helper::getPagination($url,$page,$totalpage);
			include 'Views/monhoc/index.php';
		}
		
	}
	public function edit()
	{
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		$active='quanly';
		$objMonHoc=new MonHoc;
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$data=array(
				'ten'=>$_POST['tenmonhoc'],
				'sotinchi'=>$_POST['sotinchi']
				);	
			if ($objMonHoc->update($_POST['id'],$data)) {
				$_SESSION['code']="success";
				$_SESSION['message']="Cập nhật thông tin môn học thành công";
				Helper::redirect(Helper::$siteurl."monhoc/index");
			}else {
				echo "Lỗi Hệ thống";
			}
		}
	}
	public function delete()
	{
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		$objMonHoc=new MonHoc;
		if (!empty($_GET['id'])) {
			$ma=$_GET['id'];
			if ($objMonHoc->issetMonHoc($ma)) {
				if ($objMonHoc->delete($ma)) {
					$_SESSION['code']="success";
					$_SESSION['message']="Xóa môn học thành công";
					Helper::redirect(Helper::$siteurl."monhoc/index");
				}
			}else {
				// error
			}
		}else {
			//error
		}
	}
}
?>




