<?php
/**
* 
*/
class SinhvienController
{
	public function index()
	{
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$success=0;
			$herror=0;
			$error = array();
			$target_dir = "uploads/tmp";
			$target_file = $target_dir . basename($_FILES["sinhvien"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

			if ($_FILES["sinhvien"]["size"] > 5000000) {
				$error[] = "Tệp PDF quá lớn";
				$uploadOk = 0;
			}
			if ($imageFileType != "xlsx") {
				$error[] = "Tệp sai định dạng";
				$uploadOk = 0;
			}
			if ($uploadOk == 0) {
				$error[] = "Tệp chưa được upload";
			} else {
				if (move_uploaded_file($_FILES["sinhvien"]["tmp_name"], $target_file)) {
            //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
					$objPhpexcel=new PHPExcel;
					$tmpfname = $target_file;
					$excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
					$excelObj = $excelReader->load($tmpfname);
					$worksheet = $excelObj->getSheet(0);
					$lastRow = $worksheet->getHighestRow();

					for ($row = 1; $row <= $lastRow; $row++) {
						$ma = $worksheet->getCell('A' . $row)->getValue();
						$ten = $worksheet->getCell('B' . $row)->getValue();
						$lop_ql = $worksheet->getCell('C' . $row)->getValue();
						$objSinhvien=new SinhVien;

						$data = array(
							'msv' => $ma,
							'tensv' => $ten,
							'lop_ql' => $lop_ql
							);
						if (!$objSinhvien->issetSinhvien($ma)) {
							if ($objSinhvien->add($data)) {
								$success++;
							}else {
								$herror++;
							}
						} else {
							$herror++;
						}
					}
				} else {
					$error[] = "Tệp chưa được upload";
				}
			}
			unlink($target_file);
		}
		if (!empty($_GET['key'])) {
			if (!empty($_GET['page'])) {
				$page=$_GET['page'];
			}else {
				$page=1;
			}
			$objSinhvien=new SinhVien;
			$where="msv like '%".$_GET['key']."%' ";
			$where.=" OR tensv like '%".$_GET['key']."%' ";
			$where.="OR lop_ql like '%".$_GET['key']."%' ";
			$sinhviens=$objSinhvien->getlist($where,$page);
			$totalpage=$objSinhvien->getpagewhere($where);
			$url=Helper::$siteurl."?controller=sinhvien&action=index&key={$_GET['key']}&page=";
			$phantrang=Helper::getPagination($url,$page,$totalpage);
			include 'Views/sinhvien/index.php';
		}else{
			if (!empty($_GET['id'])) {
				$page=$_GET['id'];
			}else {
				$page=1;
			}
			$objSinhvien=new SinhVien;
			$sinhviens=$objSinhvien->all($page);
			if (count($sinhviens)==0) {
				//Helper::redirect(Helper::$siteurl);
			}
			$totalpage=$objSinhvien->getpage();
			$url=Helper::$siteurl."sinhvien/index/";
			$phantrang=Helper::getPagination($url,$page,$totalpage);
			$active='quanly';

			include 'Views/sinhvien/index.php';
		}
		
	}
	public function edit()
	{
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		$objSinhvien=new Sinhvien;
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			$data=array(
				'tensv'=>$_POST['tensv'],
				'lop_ql'=>$_POST['lop_ql']
				);	
			if ($objSinhvien->update($_POST['id'],$data)) {
				$_SESSION['code']="success";
				$_SESSION['message']="Chỉnh sửa thông tin thành công ";
				Helper::redirect(Helper::$siteurl."sinhvien/index");
			}else {
				echo "Lỗi hệ thống";
			}
		}
		
	}
	public function delete()
	{
		SESSION::start();
		$objUser=new User;
		if (!Helper::isAdmin()) {
			//Helper::redirect(Helper::$siteurl."page/login");
			include 'Views/pages/permission.php';
			return;
		}
		$objSinhvien=new Sinhvien;
		if (!empty($_GET['id'])) {
			$msv=$_GET['id'];
			if ($objSinhvien->issetSinhvien($msv)) {
				if ($objSinhvien->delete($msv)) {
					$_SESSION['code']="success";
					$_SESSION['message']="Xóa sinh viên thành công ";
					Helper::redirect(Helper::$siteurl."sinhvien/index");
				}
			}else {
				// error
			}
		}else {
			//error
		}
	}
}
?>




