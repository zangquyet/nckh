<?php 
/**
 * 
 */
 class Vanbang
 {
 	
 	private $table="vanbang";
	public function all($page)
	{
		$num=Helper::$num_per_page;
		$offset=($page-1)*$num;
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} LIMIT {$num} OFFSET {$offset}";
		$result=mysqli_query($link,$sql);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		Db::close($link);
		return $list;
	}
	public function find($key,$page)
	{
		$num=Helper::$num_per_page;
		$offset=($page-1)*$num;
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE masinhvien LIKE '%{$key}%' OR tensv LIKE '%{$key}%' OR sohieubang LIKE '%{$key}%' LIMIT {$num} OFFSET {$offset}";
		$result=mysqli_query($link,$sql);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		Db::close($link);
		return $list;
	}
	public function getVanBang($id)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE id={$id}";
		$result=mysqli_query($link,$sql);
		 $row=mysqli_fetch_assoc($result);
		 return $row;
	}
	public function getVanBangbymsv($msv)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE masinhvien='{$msv}'";
		$result=mysqli_query($link,$sql);
		 $row=mysqli_fetch_assoc($result);
		 return $row;
	}
	public function getVanBangbyshb($shb)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE sohieubang='{$shb}'";
		$result=mysqli_query($link,$sql);
		 $row=mysqli_fetch_assoc($result);
		 return $row;
	}
	public function getVanBangbyhoten($hoten,$ngaysinh)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE tensv='{$hoten}' AND ngaysinh='{$ngaysinh}'";
		$result=mysqli_query($link,$sql);
		 $row=mysqli_fetch_assoc($result);
		 return $row;
	}

	public function issetvanbang($id)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE id={$id}";
		$result=mysqli_query($link,$sql);
		return mysqli_num_rows($result)==1?true:false;
	}
	public function issetvanbangmavb($mvb)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE sohieubang='{$mvb}'";
		$result=mysqli_query($link,$sql);
		return mysqli_num_rows($result)==1?true:false;
	}
	public function issetvanbanghoten($hoten,$ngaysinh)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE tensv='{$hoten}' AND ngaysinh='{$ngaysinh}'";
		$result=mysqli_query($link,$sql);
		return mysqli_num_rows($result)==1?true:false;
	}
	public function issetvanbangmasv($msv)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE masinhvien='{$msv}'";
		$result=mysqli_query($link,$sql);
		return mysqli_num_rows($result)==1?true:false;
	}
	public function xoavb($id)
	{
		$sql="DELETE FROM {$this->table} WHERE id={$id}";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
 } ?>