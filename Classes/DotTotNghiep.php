<?php 

class DotTotNghiep
{
	private $table= "dottotnghiep";
	private $last_id=0;
	public function all($page)
	{
		$num=Helper::$num_per_page;
		$offset=($page-1)*$num;
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} LIMIT {$num} OFFSET {$offset}";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		Db::close($link);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		return $list;
	}
	public function update($id,$data)
	{
		$link=Db::getInstance();
		$field=[];
		$val=[];
		$update="";
		$sql="UPDATE {$this->table} SET ";
		foreach ($data as $key => $value) {
			$update.=$key."='".$value."',";
		}
		$update=substr($update, 0,-1);
		$sql=$sql.$update." WHERE id='{$id}'";
		//die ($sql);
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else{
			echo mysqli_error($link);
			Db::close($link);
			return false;
		}	
	}
	public function getpage()
	{
		$num=Helper::$num_per_page;
		$link=Db::getInstance();
		$sql="SELECT id FROM {$this->table} ";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		$total=mysqli_num_rows($result);
		Db::close($link);
		return ceil($total/$num);
	}

	public function getList($where)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE {$where} limit 10";
		$result=mysqli_query($link,$sql);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		Db::close($link);
		return $list;
	}
	public function getdtn($id)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE id={$id}";
		$result=mysqli_query($link,$sql);
		 $row=mysqli_fetch_assoc($result);
		 return $row;
	}
	public function getPostbyfolder($id)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE img_folder='{$id}'";
		$result=mysqli_query($link,$sql);
		 $row=mysqli_fetch_assoc($result);
		 Db:close($link);
		 return $row;
	}
	public function issetDTN($id)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE id={$id}";
		$result=mysqli_query($link,$sql);
		return mysqli_num_rows($result)==1?true:false;
	}
	public function delete($id)
	{
		$sql="DELETE FROM {$this->table} WHERE id={$id}";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	public function add($array)
	{
		$link=Db::getInstance();
		$field=[];
		$val=[];
		foreach ($array as $key => $value) {
			$field[]=$key;
			$val[]="'".$value."'";
		}
		$values=implode(',', $val);
		$fie=implode(',', $field);
		$sql="INSERT INTO {$this->table} ({$fie}) VALUES ($values)";
		if (mysqli_query($link,$sql)) {
			$this->last_id = mysqli_insert_id($link);

			Db::close($link);
			return true;
		}else{
			echo mysqli_error($link);
			Db::close($link);
			return false;
		}	
	}
	public function getlastid()
	{
		return $this->last_id;
	}
}

?>

