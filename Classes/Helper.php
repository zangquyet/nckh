<?php 
/**
 * summary
 */
class Helper
{
    /**
     * summary
     */
    
    public static $siteurl="http://hand.dev/";
	public static $num_per_page=10;
    public static function get_admin_after_login_site()
    {
        return self::$siteurl."dottotnghiep/index";
    }
    public static function get_ktv_after_login_site()
    {
        return self::$siteurl."bangdiem/index";
    }

/*    public static function loadMenu($active)
    {
        if (self::isAdmin()) {
            return "
                <li class=\"<?=(isset($active)&&$active=='timkiemvanbang')?\"active\":\"\" ?>\"\" ><a href=\"".self::$siteurl."page/timkiemvanbang\">Tìm kiếm văn bằng</a></li>
                            <li class=\"<?=(isset($active)&&$active=='login')?\"active\":\"\" ?>\"\" ><a href=\"".self::$siteurl."page/login\">Đăng nhập</a></li>
            ";
        }else {
             return "
                <li class=\"<?=(isset($active)&&$active=='timkiemvanbang')?\"active\":\"\" ?>\"\" ><a href=\"".self::$siteurl."page/timkiemvanbang\">Tìm kiếm văn bằng</a></li>
                            <li class=\"<?=(isset($active)&&$active=='login')?\"active\":\"\" ?>\"\" ><a href=\"".self::$siteurl."page/login\">Đăng nhập</a></li>
            ";
        }
    }*/


    public static function getyear()
    {
        return date("Y");
    }
    public function getCurrentdate()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        return date("Y-m-d H:i:s");

    }
    public function getDate($date)
    {
        $phpdate = strtotime( $date );
        $mysqldate = date( 'H:i d-m-Y', $phpdate );
        return $mysqldate;
    }
    public function getonlyDate($date)
    {
        $phpdate = strtotime( $date );
        $mysqldate = date( 'd-m-Y', $phpdate );
        return $mysqldate;
    }
    public static function loadDropDown()
    {
        if (self::isAdmin()) {
            return "
                <li><a href=\"".self::$siteurl."page/logout\">Đăng xuất</a></li>
            ";
        }else {
            return "
                <li><a href=\"".self::$siteurl."page/logout\">Đăng xuất</a></li>
            ";
        }
    }
    public function getPagination($url,$current,$total)
    {
        if ($total==1) {
            return;
        }
    	if ($current==1) {
    		$return="<ul class=\"pagination\">"
		    		."<li class='active'><a href=\"javascript:void(0)\">Trang đầu</a></li>"
		    		."<li><a href=\"javascript:void(0)\">Trang trước</a></li>"
		    		."<li><a href=\"".$url.($current+1)."\">Trang sau</a></li>"
		    		."<li><a href=\"".$url.$total."\">Trang Cuối</a></li>"
		    	."</ul>";
    	}elseif ($current==$total) {
    		$return="<ul class=\"pagination\">"
		    		."<li><a href=\"".$url."1\">Trang đầu</a></li>"
		    		."<li><a href=\"".$url.($current-1)."\">Trang trước</a></li>"
		    		."<li><a href=\"javascript:void(0)\">Trang sau</a></li>"
		    		."<li class='active'><a href=\"javascript:void(0)\">Trang Cuối</a></li>"
		    	."</ul>";
    	}else {
    		$return="<ul class=\"pagination\">"
		    		."<li><a href=\"".$url."1\">Trang đầu</a></li>"
		    		."<li><a href=\"".$url.($current-1)."\">Trang trước</a></li>"
		    		."<li><a href=\"".$url.($current+1)."\">Trang sau</a></li>"
		    		."<li><a href=\"".$url.$total."\">Trang Cuối</a></li>"
		    	."</ul>";
    	}
    	
    	return $return;
    }
    public function getBreadcrum()
    {
        $controller= $_GET['controller'];
        $action= $_GET['action'];
        switch ($controller) {
            case 'bangdiem':
                switch ($action) {
                    case 'edit':
                        ?>
                        <ol class="breadcrumb">
                            <li>
                                <a href="#">Bảng điểm</a>
                            </li>
                        
                            <li class="active">Chỉnh sửa</li>
                        </ol>
                        <?php
                        break;
                    
                    default:
                    ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Quản lý</a>
                            </li>
                        
                            <li class="active">Bảng điểm</li>
                        </ol>
                    <?php
                        break;
                }
                break;
            case 'diemtotnghiep':
                switch ($action) {
                    case 'edit':
                        ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Quản lý</a>
                            </li>  <li>
                                <a href="#">Điểm tốt nghiệp</a>
                            </li>
                        
                            <li class="active">Chỉnh sửa</li>
                        </ol>
                    <?php
                        break;
                    
                    default:
                          ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Quản lý</a>
                            </li>
                        
                            <li class="active">Điểm tốt nghiệp</li>
                        </ol>
                    <?php
                        break;
                }
                break;

            case 'sinhvien':
                switch ($action) {
                    case 'edit':
                            ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Quản lý</a>
                            </li><li>
                                <a href="#">Sinh viên</a>
                            </li>
                        
                            <li class="active">Chỉnh sửa thông tin</li>
                        </ol>
                    <?php
                        break;
                    
                    default:
                               ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Quản lý</a>
                            </li>
                        
                            <li class="active">Sinh viên</li>
                        </ol>
                    <?php
                        break;
                }
                break;


            case 'monhoc':
                switch ($action) {
                    case 'edit':
                                 ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Quản lý</a>
                            </li><li>
                                <a href="#">Môn học</a>
                            </li>
                        
                            <li class="active">Chỉnh sửa thông tin</li>
                        </ol>
                    <?php
                        break;
                    
                    default:
                                   ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Quản lý</a>
                            </li>
                        
                            <li class="active">Môn Học</li>
                        </ol>
                    <?php
                        break;
                }
                break;

            case 'vanbang':
                switch ($action) {
                    case 'suavanbang':
                                     ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Quản lý</a>
                            </li><li>
                                <a href="#">Văn Bằng</a>
                            </li>
                        
                            <li class="active">Chỉnh sửa thông tin</li>
                        </ol>
                    <?php
                        break;
                    
                    default:
                        
                                   ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Quản lý</a>
                            </li>
                        
                            <li class="active">Văn Bằng</li>
                        </ol>
                    <?php
                        break;
                }
                break;
            case 'user':
                switch ($action) {
                    case 'edit':
                                          ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Quản lý</a>
                            </li><li>
                                <a href="#">Người dùng</a>
                            </li>
                        
                            <li class="active">Chỉnh sửa thông tin</li>
                        </ol>
                    <?php
                        break;
                    
                    default:
                                     ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Quản lý</a>
                            </li>
                        
                            <li class="active">Người dùng</li>
                        </ol>
                    <?php
                        break;
                }
                break;

            case 'dottotnghiep':
                switch ($action) {
                    case 'detail':
                                         ?>
                    <ol class="breadcrumb">
                            
                             <li>
                                <a href="#">Xét duyệt tốt nghiệp</a>
                            </li>
                        
                            <li class="active">Chi tiết</li>
                        </ol>
                    <?php
                        break;
                    
                    default:
                        # code...
                        break;
                }
                break;

            default:
                # code...
                break;
        }
    }
    public static function redirect($localtion)
    {
    	header("Location:{$localtion}");
    }
    
    public function getshorterstring($string,$length)
    {
    	return mb_substr($string,0,$length,'UTF-8');
    }

    public static function time_ago($created_time) {
    date_default_timezone_set('Asia/Calcutta'); //Change as per your default time
    $str = strtotime($created_time);
    $today = strtotime(date('Y-m-d H:i:s'));

    // It returns the time difference in Seconds...
    $time_differnce = $today - $str;

    // To Calculate the time difference in Years...
    $years = 60 * 60 * 24 * 365;

    // To Calculate the time difference in Months...
    $months = 60 * 60 * 24 * 30;

    // To Calculate the time difference in Days...
    $days = 60 * 60 * 24;

    // To Calculate the time difference in Hours...
    $hours = 60 * 60;

    // To Calculate the time difference in Minutes...
    $minutes = 60;

    if (intval($time_differnce / $years) > 1) {
    	return intval($time_differnce / $years) . " năm trước";
    } else if (intval($time_differnce / $years) > 0) {
    	return intval($time_differnce / $years) . " năm trước";
    } else if (intval($time_differnce / $months) > 1) {
    	return intval($time_differnce / $months) . " tháng trước";
    } else if (intval(($time_differnce / $months)) > 0) {
    	return intval(($time_differnce / $months)) . " tháng trước";
    } else if (intval(($time_differnce / $days)) > 1) {
    	return intval(($time_differnce / $days)) . " ngày trước";
    } else if (intval(($time_differnce / $days)) > 0) {
    	return intval(($time_differnce / $days)) . " ngày trước";
    } else if (intval(($time_differnce / $hours)) > 1) {
    	return intval(($time_differnce / $hours)) . " giờ trước";
    } else if (intval(($time_differnce / $hours)) > 0) {
    	return intval(($time_differnce / $hours)) . " giờ trước";
    } else if (intval(($time_differnce / $minutes)) > 1) {
    	return intval(($time_differnce / $minutes)) . " phút trước";
    } else if (intval(($time_differnce / $minutes)) > 0) {
    	return intval(($time_differnce / $minutes)) . " phút trước";
    } else if (intval(($time_differnce)) > 1) {
    	return intval(($time_differnce)) . " giây trước";
    } else {
    	return "Vài giây trước";
    }
}

    public static function filter($data) {
    	$data = trim($data);
    	$data = stripslashes($data);
    	$data = htmlspecialchars($data);
    	return $data;
    }
    public static function islogin()
    {
        if (isset($_SESSION['id'])) {
            return true;
        }
        return false;
    }
    public static function isAdmin()
    {
        if (empty($_SESSION['type'])) {
            return false;
        }
        if ($_SESSION['type']==1) {
            return true;
        }
        return false;
    }
    public static function isThuVien()
    {
        if (empty($_SESSION['type'])) {
            return false;
        }
        if ($_SESSION['type']==4) {
            return true;
        }
        return false;
    }
    public static function isKhoa()
    {
        if (empty($_SESSION['type'])) {
            return false;
        }
        if ($_SESSION['type']==5) {
            return true;
        }
        return false;
    }
    public static function isDang()
    {
        if (empty($_SESSION['type'])) {
            return false;
        }
        if ($_SESSION['type']==6) {
            return true;
        }
        return false;
    }

    public static function isTaiVu()
    {
        if (empty($_SESSION['type'])) {
            return false;
        }
        if ($_SESSION['type']==3) {
            return true;
        }
        return false;
    }
    public static function isKTV()
    {
        if (empty($_SESSION['type'])) {
            return false;
        }
        if ($_SESSION['type']==2) {
            return true;
        }
        return false;
    }

    public static function isCTSV()
    {
        if (empty($_SESSION['type'])) {
            return false;
        }
        if ($_SESSION['type']==7) {
            return true;
        }
        return false;
    }

}

?>