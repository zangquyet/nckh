<?php 

class SinhvienTotNghiep
{
	private $table= "sinhvientotnghiep";
	public function all($page)
	{
		$num=Helper::$num_per_page;
		$offset=($page-1)*$num;
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} LIMIT {$num} OFFSET {$offset}";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		Db::close($link);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		return $list;
	}
	public function timkiem($dtn_id,$key)
	{
		
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE id_dtn={$dtn_id} AND (masinhvien like '%{$key}%' OR tensv LIKE '%{$key}%') LIMIT 50";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		Db::close($link);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		return $list;
	}
	public function getall()
	{
		$link=Db::getInstance();
		
		$sql="SELECT * FROM {$this->table}";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		Db::close($link);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		return $list;
	}
	public function gettotalsinhvien($id)
	{
		$num=Helper::$num_per_page;
		$link=Db::getInstance();
		$sql="SELECT id FROM {$this->table} WHERE id_dtn={$id} ";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		return $total=mysqli_num_rows($result);
	}
	public function gettotalsinhviendudk($id)
	{
		$num=Helper::$num_per_page;
		$link=Db::getInstance();
		$sql="SELECT id FROM {$this->table} WHERE"
		." (id_dtn={$id} AND hocphi_stt=1 AND giaotrinh_stt=1 AND khoa_stt=1 AND dang_stt=1 AND is_dangvien=1)"
		." OR (id_dtn={$id} AND hocphi_stt=1 AND giaotrinh_stt=1 AND khoa_stt=1 AND is_dangvien=0)";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		return $total=mysqli_num_rows($result);
	}
	public function gettotalsinhvienhthp($id)
	{
		$num=Helper::$num_per_page;
		$link=Db::getInstance();
		$sql="SELECT id FROM {$this->table} WHERE id_dtn={$id} AND hocphi_stt=1";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		return $total=mysqli_num_rows($result);
	}
	public function gettotalsinhvienhttrasach($id)
	{
		$num=Helper::$num_per_page;
		$link=Db::getInstance();
		$sql="SELECT id FROM {$this->table} WHERE id_dtn={$id} AND giaotrinh_stt=1";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		return $total=mysqli_num_rows($result);
	}
	public function gettotalsinhvienhtthutuckhoa($id)
	{
		$num=Helper::$num_per_page;
		$link=Db::getInstance();
		$sql="SELECT id FROM {$this->table} WHERE id_dtn={$id} AND khoa_stt=1";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		return $total=mysqli_num_rows($result);
	}
	public function gettotalsinhvienhtthutuccshdang($id)
	{
		$num=Helper::$num_per_page;
		$link=Db::getInstance();
		$sql="SELECT id FROM {$this->table} WHERE id_dtn={$id} AND dang_stt=1";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		return $total=mysqli_num_rows($result);
	}
	public function gettotalsinhvien_ruthoso($id)
	{
		$num=Helper::$num_per_page;
		$link=Db::getInstance();
		$sql="SELECT id FROM {$this->table} WHERE id_dtn={$id} AND tths=1";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		return $total=mysqli_num_rows($result);
	}
	public function getpage()
	{
		$num=50;
		$link=Db::getInstance();
		$sql="SELECT id FROM {$this->table} ";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		$total=mysqli_num_rows($result);
		Db::close($link);
		return ceil($total/$num);
	}

	public function getpagewhere($where)
	{
		$num=50;
		$link=Db::getInstance();
		$sql="SELECT id FROM {$this->table} WHERE {$where}";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		$total=mysqli_num_rows($result);
		Db::close($link);
		return ceil($total/$num);
	}

	public function getList($where,$page)
	{
		$num=50;
		$offset=($page-1)*$num;
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE {$where} LIMIT 50 OFFSET {$offset}";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		Db::close($link);
		return $list;
	}
	public function getSinhvienTotNghiepDuDK($id_dtn)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE id_dtn={$id_dtn} AND hocphi_stt=1 AND giaotrinh_stt=1 AND khoa_stt=1 AND dang_stt=1";
		$result=mysqli_query($link,$sql);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		Db::close($link);
		return $list;
	}
	public function getSinhvienTotThieuHocPhi($id_dtn)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE id_dtn={$id_dtn} AND hocphi_stt=0";
		$result=mysqli_query($link,$sql);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		Db::close($link);
		return $list;
	}
	public function getSinhvienMuonSach($id_dtn)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE giaotrinh_stt=0";
		$result=mysqli_query($link,$sql);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		Db::close($link);
		return $list;
	}

	public function isset($msv)
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table} WHERE masinhvien='{$msv}'";
		$result=mysqli_query($link,$sql);
		return mysqli_num_rows($result)==1?true:false;
	}
	public function delete($id)
	{
		$sql="DELETE FROM {$this->table} WHERE id={$id}";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	public function delete_id_dtn($id)
	{
		$sql="DELETE FROM {$this->table} WHERE id_dtn={$id}";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	public function add($array)
	{
		$link=Db::getInstance();
		$field=[];
		$val=[];
		foreach ($array as $key => $value) {
			$field[]=$key;
			$val[]="'".$value."'";
		}
		$values=implode(',', $val);
		$fie=implode(',', $field);
		$sql="INSERT INTO {$this->table} ({$fie}) VALUES ($values)";
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else{
			echo mysqli_error($link);
			Db::close($link);
			return false;
		}	
	}
	public function update_hocphi($id)
	{
		session_start();
		$id2=$_SESSION['id'];
		//$date=now();
		$sql="UPDATE {$this->table} SET hocphi_stt=1,taivu_id={$id2},taivu_time=NOW() WHERE id={$id} ";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	
	public function uncheck_hocphi($id)
	{
		session_start();
		$id2=$_SESSION['id'];
		//$date=now();
		$sql="UPDATE {$this->table} SET hocphi_stt=0,taivu_id={$id2},taivu_time=NOW() WHERE id={$id} ";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	public function update_tths($id)
	{
		session_start();
		$id2=$_SESSION['id'];
		//$date=now();
		$sql="UPDATE {$this->table} SET tths=1 WHERE id={$id} ";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	
	public function uncheck_tths($id)
	{
		session_start();
		$id2=$_SESSION['id'];
		//$date=now();
		$sql="UPDATE {$this->table} SET tths=0 WHERE id={$id} ";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	public function update_dang($id)
	{
		session_start();
		$id2=$_SESSION['id'];
		//$date=now();
		$sql="UPDATE {$this->table} SET dang_stt=1,dang_id={$id2},dang_time=NOW() WHERE id={$id} ";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	
	public function uncheck_dang($id)
	{
		session_start();
		$id2=$_SESSION['id'];
		//$date=now();
		$sql="UPDATE {$this->table} SET dang_stt=0,dang_id={$id2},dang_time=NOW() WHERE id={$id} ";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	
	public function update_giaotrinh($id)
	{
		session_start();
		$id2=$_SESSION['id'];
		//$date=now();
		$sql="UPDATE {$this->table} SET giaotrinh_stt=1,thuvien_id={$id2},thuvien_time=NOW() WHERE id={$id} ";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	public function uncheck_giaotrinh($id)
	{
		session_start();
		$id2=$_SESSION['id'];
		//$date=now();
		$sql="UPDATE {$this->table} SET giaotrinh_stt=0,thuvien_id={$id2},thuvien_time=NOW() WHERE id={$id} ";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	public function update_khoa($id)
	{
		session_start();
		$id2=$_SESSION['id'];
		//$date=now();
		$sql="UPDATE {$this->table} SET khoa_stt=1,khoa_id={$id2},khoa_time=NOW() WHERE id={$id} ";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	public function uncheck_khoa($id)
	{
		session_start();
		$id2=$_SESSION['id'];
		//$date=now();
		$sql="UPDATE {$this->table} SET khoa_stt=0,khoa_id={$id2},khoa_time=NOW() WHERE id={$id} ";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
	public function update($id_dtn,$msv,$data)
	{
		$link=Db::getInstance();
		$field=[];
		$val=[];
		$update="";
		$sql="UPDATE {$this->table} SET ";
		foreach ($data as $key => $value) {
			$update.=$key."='".$value."',";
		}
		$update=substr($update, 0,-1);
		$sql=$sql.$update." WHERE id_dtn={$id_dtn} AND masinhvien='{$msv}'";
		//die($sql);
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else{
			echo mysqli_error($link);
			Db::close($link);
			return false;
		}	
	}
	public function update_theo_id($id,$data)
	{
		$link=Db::getInstance();
		$field=[];
		$val=[];
		$update="";
		$sql="UPDATE {$this->table} SET ";
		foreach ($data as $key => $value) {
			$update.=$key."='".$value."',";
		}
		$update=substr($update, 0,-1);
		$sql=$sql.$update." WHERE id='{$id}'";
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else{
			echo mysqli_error($link);
			Db::close($link);
			return false;
		}	
	}
}

?>

