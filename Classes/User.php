<?php 
/**
* 
*/
class User
{
	private $table="user";
	public function all()
	{
		$link=Db::getInstance();
		$sql="SELECT * FROM {$this->table}";
		//echo $sql;
		$result=mysqli_query($link,$sql);
		Db::close($link);
		$list=array();
		while($row=mysqli_fetch_assoc($result)){
			$list[]=$row;
		}
		return $list;
	}
	public function issetUser($username)
	{
		$sql="SELECT id FROM {$this->table} WHERE username='{$username}'";
		$link=Db::getInstance();
		$result=mysqli_query($link,$sql);
		$rowcount=mysqli_num_rows($result);
		Db::close($link);
		return $rowcount>0?true:false;
	}
	public function issetUserID($id)
	{
		$sql="SELECT id FROM {$this->table} WHERE id={$id}";
		$link=Db::getInstance();
		$result=mysqli_query($link,$sql);
		$rowcount=mysqli_num_rows($result);
		Db::close($link);
		return $rowcount>0?true:false;
	}

	public function add($array)
	{
		$link=Db::getInstance();
		$field=[];
		$val=[];
		foreach ($array as $key => $value) {
			$field[]=$key;
			$val[]="'".$value."'";
		}
		$values=implode(',', $val);
		$fie=implode(',', $field);
		$sql="INSERT INTO {$this->table} ({$fie}) VALUES ($values)";
		if (mysqli_query($link,$sql)) {
			Db::close($link);
		 	return true;
		 }else{
		 	echo mysqli_error($link);
		 	Db::close($link);
		 	return false;
		 }	
	}
	public function update($id,$data)
	{
		$link=Db::getInstance();
		$field=[];
		$val=[];
		$update="";
		$sql="UPDATE {$this->table} SET ";
		foreach ($data as $key => $value) {
			$update.=$key."='".$value."',";
		}
		$update=substr($update, 0,-1);
		$sql=$sql.$update." WHERE id='{$id}'";
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else{
			echo mysqli_error($link);
			Db::close($link);
			return false;
		}	
	}
	public function updatepassword($id,$pass)
	{
		$pass=md5($pass);
		$link=Db::getInstance();
		$sql="UPDATE {$this->table} SET password='{$pass}' WHERE id={$id}";
		if (mysqli_query($link,$sql)) {
			Db::close($link);
		 	return true;
		 }else{
		 	echo mysqli_error($link);
		 	Db::close($link);
		 	return false;
		 }	
	}
	public function getUserByUsername($username)
	{
		$sql="SELECT * FROM {$this->table} WHERE username='{$username}'";
		$link=Db::getInstance();
		if ($result=mysqli_query($link,$sql)) {
			Db::close($link);
			return mysqli_fetch_assoc($result);
		}else{
			Db::close($link);
			echo mysqli_error($link);
		}
	}

	public function getUserById($id)
	{
		$sql="SELECT * FROM {$this->table} WHERE id='{$id}'";
		$link=Db::getInstance();
		if ($result=mysqli_query($link,$sql)) {
			Db::close($link);
			return mysqli_fetch_assoc($result);
		}else{

			echo mysqli_error($link);
			Db::close($link);
		}
	}
	public function getUsernameById($id)
	{
		$sql="SELECT * FROM {$this->table} WHERE id='{$id}'";
		$link=Db::getInstance();
		if ($result=mysqli_query($link,$sql)) {
			Db::close($link);
			$return=mysqli_fetch_assoc($result);
			return $return['username'];
		}else{

			echo mysqli_error($link);
			Db::close($link);
		}
	}
	public function isUser($username,$pass)
	{
		$pass=md5($pass);
		$sql="SELECT * FROM {$this->table} WHERE username='{$username}' AND password='{$pass}'";
		$link=Db::getInstance();
		if ($result=mysqli_query($link,$sql)) {
			$rowcount=mysqli_num_rows($result);
			Db::close($link);
			return $rowcount>0?true:false;	
		}else{
			echo mysqli_error($link);
			Db::close($link);
		}
	}

	public function islogin()
	{
		if (isset($_SESSION['id'])) {
			return true;
		}
		return false;
	}
	public function isAdmin()
	{
		if (isset($_SESSION['level'])==1) {
			return true;
		}
		return false;
	}
	public function delete($id)
	{
		$sql="DELETE FROM {$this->table} WHERE id={$id}";
		$link=Db::getInstance();
		if (mysqli_query($link,$sql)) {
			Db::close($link);
			return true;
		}else {
			Db::close($link);
			return false;
		}
	}
}

?>