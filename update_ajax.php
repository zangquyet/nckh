<?php
if ($_SERVER['REQUEST_METHOD']=="POST") {
	include 'Classes/Db.php';
	include 'Classes/User.php';
	include 'Classes/Helper.php';
	include 'Classes/SinhvienTotNghiep.php';
	$objSinhvien =new SinhvienTotNghiep;
	$objUser =new User;
	$action=$_POST['action'];
	if ($action=="update_hocphi") {
		$id=$_POST['myid'];
		if ($objSinhvien->update_hocphi($id)) {
			$result=array(
				'success'=>true
				);
			//echo $list;
			
			echo json_encode($result);
		}else {
			echo "Update False";
		}
	}
	if ($action=="uncheck_hocphi") {
		$id=$_POST['myid'];
		echo $id;
		if ($objSinhvien->uncheck_hocphi($id)) {
			$result=array(
				'success'=>true
				);
			//echo json_encode($result);
			echo "thanhcong";
		}else {
			echo "Update False";
		}
	}
	if ($action=="update_dang") {
		$id=$_POST['myid'];
		if ($objSinhvien->update_dang($id)) {
			$result=array(
				'success'=>true
				);
			//echo $list;
			
			echo json_encode($result);
		}else {
			echo "Update False";
		}
	}
	if ($action=="uncheck_dang") {
		$id=$_POST['myid'];
		echo $id;
		if ($objSinhvien->uncheck_dang($id)) {
			$result=array(
				'success'=>true
				);
			//echo json_encode($result);
			echo "thanhcong";
		}else {
			echo "Update False";
		}
	}
	if ($action=="update_giaotrinh") {
		$id=$_POST['myid'];
		if ($objSinhvien->update_giaotrinh($id)) {
			$result=array(
				'success'=>true
				);
			//echo $list;
			
			echo json_encode($result);
		}else {
			echo "Update False";
		}
	}
	if ($action=="uncheck_giaotrinh") {
		$id=$_POST['myid'];
		if ($objSinhvien->uncheck_giaotrinh($id)) {
			$result=array(
				'success'=>true
				);
			echo json_encode($result);
		}else {
			echo "Update False";
		}
	}
	if ($action=="update_khoa") {
		$id=$_POST['myid'];
		if ($objSinhvien->update_khoa($id)) {
			$result=array(
				'success'=>true
				);
			//echo $list;
			
			echo json_encode($result);
		}else {
			echo "Update False";
		}
	}
	if ($action=="uncheck_khoa") {
		$id=$_POST['myid'];
		if ($objSinhvien->uncheck_khoa($id)) {
			$result=array(
				'success'=>true
				);
			echo json_encode($result);
		}else {
			echo "Update False";
		}
	}
	if ($action=="update_tths") {
		$id=$_POST['myid'];
		if ($objSinhvien->update_tths($id)) {
			$result=array(
				'success'=>true
				);
			//echo $list;
			
			echo json_encode($result);
		}else {
			echo "Update False";
		}
	}
	if ($action=="uncheck_tths") {
		$id=$_POST['myid'];
		if ($objSinhvien->uncheck_tths($id)) {
			$result=array(
				'success'=>true
				);
			echo json_encode($result);
		}else {
			echo "Update False";
		}
	}
	if ($action=="update_table") {
		sleep(1);
		session_start();
		$id=$_POST['id_dtn'];
		$page=$_POST['page'];
		$key=$_POST['key']!=NULL?$_POST['key']:"";
		$where="id_dtn=".$id." AND (tensv LIKE '%{$key}%' OR masinhvien LIKE '%{$key}%')";
		$sinhviens=$objSinhvien->getList($where,$page);
		?>
		<div class="row">
				<div class="col-md-12">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>STT</th>
								<th>Mã</th>
								<th>Họ tên</th>
								<th>Lớp</th>
								<th>Khoa</th>
								<?php if (Helper::isTaiVu()): ?>
									<th class="hocphi"><input type="checkbox"  name="" id="check1">TTHP</th>
									<th>Thời gian</th>
								<?php endif ?>
								<?php if (Helper::isThuVien()): ?>
									<th><input type="checkbox"  name="" id="check2">TTTV</th>
									<th>Thời gian</th>
								<?php endif ?>
								<?php if (Helper::isCTSV()): ?>
									<th>TT Hồ sơ</th>
								<?php endif ?>
								<?php if (Helper::isKhoa()): ?>
									<th><input type="checkbox"  name="" id="check2">TTK</th>
									<th>Thời gian</th>
									<?php if (Helper::isKhoa()): ?>
										<th>Trạng thái hồ sơ</th>
									<?php endif ?>
								<?php endif ?>
								<?php if (Helper::isDang()): ?>
									<th><input type="checkbox"  name="" id="check2">TTCSHĐ</th>
									<th>Thời gian</th>
								<?php endif ?>

								<?php if (Helper::isAdmin()): ?>
									<td>TT Học phí</td>
									<td>TT Thư viện</td>
									<td>TT Khoa</td>
									<td>TT Chuyển SH Đảng</td>
									<td>TT hồ sơ</td>
								<?php endif ?>
							</tr>
						</thead>
						<tbody >
							<?php
							if (!empty($_GET['page'])) {
								$count=($_GET['page']-1)*50+1;
							}else {
								$count=1;
							}
							foreach ($sinhviens as $sinhvien): ?>

							<tr >
								<td><?= $count ?></td>
								<?php $count++; ?>
								<td>
									<a class="link" data-toggle="modal" href='#modal-edit-<?=$sinhvien['id']?>'><?= $sinhvien['masinhvien'] ?></a>
									<div class="modal fade" id="modal-edit-<?=$sinhvien['id']?>">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h4 class="modal-title">Chỉnh sửa thông tin sinh viên</h4>
												</div>
												<form action="/dottotnghiep/saveupdatesinhvien" method="POST">
													<div class="modal-body">
														<input type="hidden" name="id" value="<?= $sinhvien['id'] ?>">
														<input type="hidden" name="id_dtn" value="<?= $sinhvien['id_dtn'] ?>">
														<table>
															<tr>
																<td>Mã sinh viên</td>
																<td><?= $sinhvien['masinhvien'] ?></td>
															</tr>
															<tr>
																<td>Tên sinh viên</td>
																<td class="form-group"><input type="text" class="form-control" name="tensv" value="<?= $sinhvien['tensv'] ?>"></td>
															</tr>
															<tr>
																<td>Lớp</td>
																<td class="form-group"><input type="text" class="form-control" name="lop_ql" value="<?= $sinhvien['lop_ql'] ?>"></td>
															</tr>
															
															<tr>
																<td>Khoa </td>
																<td class="form-group"><input type="text" name="khoa" class="form-control" value="<?= $sinhvien['khoa'] ?>"></td>
															</tr>
															
														</table>
													</div>
													<div class="modal-footer">
														<div class="pull-left">
															<button type="button" onclick="xoa(<?= $sinhvien['id'].",'".$sinhvien['tensv']."'" ?>)" class="btn btn-danger btn-raised">Xóa</button>
														</div>
														<button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
														<button type="submit" class="btn btn-primary btn-raised">Lưu chỉnh sửa</button>
													</div>
												</form>
											</div>
										</div>
									</div>
									

								</td>

								<td class="myshow" onmouseleave="hidetooltip()"
								onmouseenter="update_tooltip(<?php 

									echo "'".$sinhvien['masinhvien']."',";
									echo "'".$sinhvien['tensv']."',";
									echo "'".$sinhvien['lop_ql']."',";
									echo "'".$sinhvien['khoa']."',";


									echo $sinhvien['hocphi_stt']==1?"'Đã hoàn thành'":"'Chưa hoàn thành'";
									echo ",";
									echo ($sinhvien['taivu_time']!=NULL)?"'".$objUser->getUsernameById($sinhvien['taivu_id'])."'":"''";
									echo ",";

									echo $sinhvien['giaotrinh_stt']==1?"'Đã hoàn thành'":"'Chưa hoàn thành'";
									echo ",";
									echo ($sinhvien['thuvien_time']!=NULL)?"'".$objUser->getUsernameById($sinhvien['thuvien_id'])."'":"''";
									echo ",";


									echo $sinhvien['khoa_stt']==1?"'Đã hoàn thành'":"'Chưa hoàn thành'";
									echo ",";
									echo ($sinhvien['khoa_time']!=NULL)?"'".$objUser->getUsernameById($sinhvien['khoa_id'])."'":"''";
									echo ",";


									echo $sinhvien['dang_stt']==1?"'Đã hoàn thành'":"'Chưa hoàn thành'";
									echo ",";
									echo ($sinhvien['dang_time']!=NULL)?"'".$objUser->getUsernameById($sinhvien['dang_id'])."'":"''";
									echo "";

									?>)"
									><?= $sinhvien['tensv'] ?></td>
									<td><?= $sinhvien['lop_ql'] ?></td>
									<td><?= Helper::getshorterstring($sinhvien['khoa'],20) ?></td>
									<?php if (Helper::isTaiVu()): ?>

										<td class="hocphi">
											<div class="checkbox">
												<label>
													<input type="checkbox" <?= $sinhvien['hocphi_stt']==1?"checked":"" ?> onchange="checkhocphi(<?= $sinhvien['id'] ?>)" name="" id="<?= $sinhvien['id'] ?>">
												</label>
											</div>
										</td>
										<td>
											<?php if ($sinhvien['taivu_time']!=NULL): ?>
												Bởi <span class="label label-success"><?= $objUser->getUsernameById($sinhvien['taivu_id']) ?></span><br>

												<?= Helper::getdate($sinhvien['taivu_time']) ?>
											<?php endif ?>
										</td>
									<?php endif ?>

									<?php if (Helper::isThuVien()): ?>

										<td>
											<div class="checkbox">
												<label>
													<input type="checkbox" class="giaotrinh" <?= $sinhvien['giaotrinh_stt']==1?"checked":"" ?> onchange="checkgiaotrinh(<?= $sinhvien['id'] ?>)" name="" id="gt<?= $sinhvien['id'] ?>">
												</label>
											</div>
										</td>
										<td>
											<?php if ($sinhvien['thuvien_time']!=NULL): ?>
												Bởi <span class="label label-success"><?= $objUser->getUsernameById($sinhvien['thuvien_id']) ?></span><br>
												<?= Helper::getdate($sinhvien['thuvien_time']) ?>
											<?php endif ?>
										</td>
									<?php endif ?>
									<?php if (Helper::isCTSV()): ?>

										<td>
											<div class="checkbox">
												<label>
													<input type="checkbox" class="tths" <?= $sinhvien['tths']==1?"checked":"" ?> onchange="checktths(<?= $sinhvien['id'] ?>)" name="" id="tths<?= $sinhvien['id'] ?>">
												</label>
											</div>
										</td>
									<?php endif ?>
									<?php if (Helper::isKhoa()): ?>

										<td>
											<div class="checkbox">
												<label>
													<input type="checkbox" class="giaotrinh" <?= $sinhvien['khoa_stt']==1?"checked":"" ?> onchange="checkkhoa(<?= $sinhvien['id'] ?>)" name="" id="khoa<?= $sinhvien['id'] ?>">
												</label>
											</div>
										</td>
										<td>
											<?php if ($sinhvien['khoa_time']!=NULL): ?>
												Bởi <span class="label label-success"><?= $objUser->getUsernameById($sinhvien['khoa_id']) ?></span><br>
												<?= Helper::getdate($sinhvien['khoa_time']) ?>
											<?php endif ?>
										</td>
										<?php if (Helper::isKhoa()): ?>

											<td>
												<?php if ($sinhvien['tths']==1): ?>
													<span class="label label-success"><i class="fa fa-check"></i></span>
												<?php else: ?>
													<span class="label label-info"><i class="fa fa-times"></i></span>
												<?php endif ?>
											</td>
										<?php endif ?>
									<?php endif ?>
									<?php if (Helper::isDang()): ?>

										<td>
											<?php if ($sinhvien['is_dangvien']==1): ?>
												<div class="checkbox">
													<label>
														<input type="checkbox" class="dang" <?= $sinhvien['dang_stt']==1?"checked":"" ?> onchange="checkdang(<?= $sinhvien['id'] ?>)" name="" id="dang<?= $sinhvien['id'] ?>">
													</label>
												</div>
											<?php else: ?>
												<i class="fa fa-ban"></i>	
											<?php endif ?>

										</td>
										<td>
											<?php if ($sinhvien['dang_time']!=NULL): ?>
												Bởi <span class="label label-success"><?= $objUser->getUsernameById($sinhvien['dang_id']) ?></span><br>
												<?= Helper::getdate($sinhvien['dang_time']) ?>
											<?php endif ?>
										</td>
									<?php endif ?>

									<?php if (Helper::isAdmin()): ?>
										<td>
											<?php
											if ($sinhvien['hocphi_stt']==1) {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-success btn-raised"><i class="fa fa-check"></i> </a>
												<?php
											}else {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-raised"><i class="fa fa-times"></i> </a>
												<?php
											}

											?>
										</td>
										<td>
											<?php
											if ($sinhvien['giaotrinh_stt']==1) {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-success btn-raised"><i class="fa fa-check"></i> </a>
												<?php
											}else {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-raised"><i class="fa fa-times"></i> </a>
												<?php
											}

											?>
										</td>
										<td>
											<?php
											if ($sinhvien['khoa_stt']==1) {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-success btn-raised"><i class="fa fa-check"></i> </a>
												<?php
											}else {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-raised"><i class="fa fa-times"></i> </a>
												<?php
											}

											?>
										</td>
										<td>
											<?php
											if ($sinhvien['dang_stt']==1) {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-success btn-raised"><i class="fa fa-check"></i> </a>
												<?php
											}else {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-raised"><i class="fa fa-times"></i> </a>
												<?php
											}

											?>
										</td>
										<td>
											<?php
											if ($sinhvien['tths']==1) {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-success btn-raised"><i class="fa fa-check"></i> </a>
												<?php
											}else {
												?>
												<a href="javascript:void(0)" class="btn btn-xs btn-danger btn-raised"><i class="fa fa-times"></i> </a>
												<?php
											}

											?>
										</td>
									<?php endif ?>
								</tr>
							<?php endforeach ?>

						</tbody>
					</table>
				</div>
			</div>
		<?php

	}

}
?>